#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}


if [[ ! -d "/var/www/html/vendor" ]]; then
  echo "Instaling dependencies"
  ( 
    cd /var/www/html/ &&
    composer install --prefer-dist --no-interaction --optimize-autoloader --no-dev
  )
fi

echo "PHP platform requirements"
(
    cd /var/www/html &&
    composer check-platform-reqs
)


if [ "$env" != "local" ]; then
    echo "Caching configuration..."
    (
        cd /var/www/html && 
        php artisan config:cache && 
        php artisan route:cache
    )
fi


exec "$@"