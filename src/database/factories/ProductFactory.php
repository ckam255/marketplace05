<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->title,
            'slug' => $this->faker->unique()->uuid,
            'barcode' => $this->faker->ean13,
            'description' => $this->faker->paragraph,
            'price' => rand(50, 100000),
            'weight' => rand(0, 100),
            'length' => rand(0, 100),
            'height' => rand(0, 100),
            'width' => rand(0, 100),
            'vat' => rand(0, 30),
            'isvisible' => rand(0, 1),
            // 'store_id' => rand(1, 10),
            'brand_id' => rand(1, 10),
            'category_id' => rand(1, 10),
            // 'status_id' => 1,
        ];
    }
}