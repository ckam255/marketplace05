<?php

namespace Database\Factories;

use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;

class StoreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Store::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company,
            'contact' => $this->faker->unique()->phoneNumber,
            'status' => rand(0, 1),
            'website' => $this->faker->url,
            'user_id' => \App\Models\User::factory(),
        ];
    }
}
