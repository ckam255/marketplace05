<?php

namespace Database\Seeders;

use App\Models\Sale;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::truncate();
        \App\Models\Store::truncate();

        // super user
        \App\Models\User::create([
            'name' => "MP1C05RU",
            'email' => "mp1c@05.ru",
            'password' => Hash::make('5T50QJfK'),
            'is_active' => true,
            'is_admin' => true,
            'is_superuser' => false
        ]);

        if (env('APP_ENV') === 'local') {
            // Бибихауз
            \App\Models\User::create([
                'name' => "Бибихауз",
                'email' => "test001@mail.ru",
                'password' => Hash::make('test001'),
                'is_active' => 1,
            ]);
            // MobisPlay
            \App\Models\User::create([
                'name' => "MobisPlay",
                'email' => "test002@mail.ru",
                'password' => Hash::make('test002'),
                'is_active' => 1,
            ]);
            // Test store
            \App\Models\User::create([
                'name' => "test",
                'email' => "test003@mail.ru",
                'password' => Hash::make('test003'),
                'is_active' => 1
            ]);
        }
    }
}
