<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Status::truncate();

        \App\Models\Status::insert([
            // Статусы Товаров
            array('id' => 1, 'title' => "На проверке", 'name' => 'checking'),
            array('id' => 2, 'title' => "С ошибками", 'name' => 'wrong'),
            array('id' => 3, 'title' => "Перемодерация", 'name' => 'remoderation'),
            array('id' => 4, 'title' => "Активен", 'name' => 'activated'),
            array('id' => 5, 'title' => "В черновике", 'name' => 'draft'),
            // Статусы продавцов маркетплейс
            array('id' => 11, 'title' => "Ждет регистрации", 'name' => 'confirmation'),
            array('id' => 12, 'title' => "Ждет проверки", 'name' => 'checking'),
            array('id' => 13, 'title' => "Ждет исправлений", 'name' => 'fixing'),
            array('id' => 14, 'title' => "Неактивен", 'name' => 'rejected'),
            array('id' => 15, 'title' => "Активен", 'name' => 'activated'),
            // Статусы заказов
            array('id' => 20, 'title' => "Продажа", 'name' => 'sale'),
            array('id' => 21, 'title' => "Возврат", 'name' => 'return'),
            array('id' => 22, 'title' => "Ожидает оплаты", 'name' => 'payment'),
            array('id' => 23, 'title' => "Доставка", 'name' => 'delivery'),
        ]);
    }
}
