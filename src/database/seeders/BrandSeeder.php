<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Brand::insert([
            [
                'id' => ucode(),
                'name' => 'Apple, Inc'
            ],
            [
                'id' => ucode(),
                'name' => 'Microsoft'
            ],
            [
                'id' => ucode(),
                'name' => 'IBM'
            ],
            [
                'id' => ucode(),
                'name' => 'Nokia'
            ],
            [
                'id' => ucode(),
                'name' => 'LG,Inc'
            ],
            [
                'id' => ucode(),
                'name' => 'DELL'
            ],
            [
                'id' => ucode(),
                'name' => 'Toshiba'
            ],
            [
                'id' => ucode(),
                'name' => 'Xiomi'
            ],
            [
                'id' => ucode(),
                'name' => 'Huawai'
            ]
        ]);
    }
}
