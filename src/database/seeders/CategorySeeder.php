<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Category::truncate();
       
        \App\Models\Category::insert(
            [
                [
                  'id' => ucode(),
                  'name' => 'Телефоны и гаджеты',
                ],
                [
                  'id'=>ucode(),
                  'name'=>'Компьютерная техника',
                ],
                [
                  'id'=>ucode(),
                  'name'=>'Офисная техника и сети',
                ],
                [
                  'id'=>ucode(),
                  'name'=>'Телевизоры, аудио, видео',
                ],
                [
                  'id' => ucode(),
                  'name'=>'Климатическая техника',
                ]
            ]
        );
        
    }
}