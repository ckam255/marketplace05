<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* заказ */
        Schema::create('orders', function (Blueprint $table) {
            $table->id('id');
            $table->string('number');
            $table->boolean('type')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->decimal('sum')->default(0);
            $table->foreignId('store_id')->nullable()->constrained(); // partner code
            $table->foreignId('status_id')->nullable()->constrained()->references('id')->on('status'); // status id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
