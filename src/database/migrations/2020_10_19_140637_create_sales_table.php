<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Продаж */
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->foreignId('order_id')->constrained()->onDelete('cascade'); // order id
            $table->foreignId('product_id')->constrained()->onDelete('cascade'); // product code
            $table->integer('quantity')->default(0); // каличесво товаров
            $table->decimal('sum')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
