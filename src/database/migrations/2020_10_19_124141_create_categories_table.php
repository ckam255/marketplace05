<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('code_1c')->unique()->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreignId('tariff_id')->unsigned()->nullable()->constrained(); // tariff id 
            $table->foreignId('created_by')->nullable()->constrained()
                ->referencies('id')->on('users'); // user_id
            $table->timestamps();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('categories');
        });

        Schema::create('category_property', function (Blueprint $table) {
            $table->id();
            $table->string('group_name')->nullable(); // characteristic
            $table->foreignId('category_id')->nullable()->constrained(); // category's id
            $table->foreignId('property_id')->nullable()->constrained(); // property id 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_property');
        Schema::dropIfExists('categories');
    }
}
