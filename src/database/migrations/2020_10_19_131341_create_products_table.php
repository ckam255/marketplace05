<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code_1c')->unique()->nullable(); // Код 1c
            $table->string('parent_id')->nullable();
            $table->string('name');  // Название товара
            $table->string('slug')->nullable();  //Артикул
            $table->string('barcode')->nullable();  // штрифкод
            $table->text('description')->nullable();  // Описние товара
            $table->decimal('price')->default(0);  // Цена товара
            $table->decimal('weight')->default(0);  // Вес
            $table->decimal('length')->default(0);  // длина
            $table->decimal('height')->default(0);  // высота
            $table->decimal('width')->default(0); // ширина
            $table->decimal('vat')->default(0);  // Нало́г на доба́вленную сто́имость (НДС) ( value Added Tax)
            $table->boolean('isvisible')->default(true);  //  Название товара
            $table->boolean('archived')->default(false);  //  Название товара
            $table->string('imported')->nullable(); // if product imported from yaml file
            $table->integer('quantity')->unsigned()->default(0);
            $table->foreignId('store_id')->nullable()->constrained(); // store id
            $table->foreignId('brand_id')->nullable()->constrained(); // brand id
            $table->foreignId('category_id')->nullable()->constrained(); // category's id
            $table->foreignId('status_id')->constrained()->references('id')->on('status'); // status id
            $table->timestamps();
        });
        Schema::create('product_property', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->nullable()->constrained()->onDelete('cascade'); // product id
            $table->foreignId('property_id')->nullable()->constrained()->onDelete('cascade'); // property id
            $table->json('value')->nullable();
            $table->boolean('is_value_id')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_property');
        Schema::dropIfExists('products');
    }
}
