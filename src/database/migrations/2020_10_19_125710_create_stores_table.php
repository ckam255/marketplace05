<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('code_1c')->unique()->nullable();
            $table->string('name');
            $table->string('contact')->nullable();
            $table->string('website')->nullable();
            $table->json('address')->nullable();
            $table->decimal('balance')->default(0);
            $table->foreignId('tariff_id')->unsigned()->nullable()->constrained(); // tariff id 
            $table->foreignId('user_id')->nullable()->constrained(); // user id
            $table->foreignId('status_id')->nullable(); // status id
            $table->timestamps();
        });
        Schema::create('legals', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->string('director')->nullable();
            $table->json('address')->nullable();
            $table->string('form')->nullable();
            $table->string('inn')->nullable();  //ИНН
            $table->string('bik')->nullable();  //БИК
            $table->string('kpp')->nullable();  //КПП
            $table->string('ogrn')->nullable();  //ОГРН
            $table->string('ogrnip')->nullable();  //ОГРНИП
            $table->string('payment_account')->nullable();  //расчетный счет компании
            $table->foreignId('store_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('worktimes', function (Blueprint $table) {
            $table->id('id');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->foreignId('store_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legals');
        Schema::dropIfExists('worktimes');
        Schema::dropIfExists('stores');
    }
}
