openapi: 3.0.1
info:
  title: "marketplace API Doc"
  description: "05.ru marketplace API Doc"
  version: 1.0.0
servers:
  - url: https://localhost:3000/api/
  - url: http://localhost:3000/api/
tags:
  - name: "Учетная запись"
  - name: Категории
  - name: Бренды
  - name: Товары
  - name: Магазины
  - name: "Заказы - Расчеты"
paths:
  /auth/login:
    post:
      tags:
        - "Учетная запись"
      summary: Авторизация
      requestBody:
        description: Авторизация параметры
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/LoginSchema"
        required: true
      responses:
        400:
          description: Bad request
        200:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UserStoreSchema"
  /auth/register:
    post:
      tags:
        - "Учетная запись"
      summary: Регистрация
      requestBody:
        description: параметры
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/RegisterSchema"
        required: true
      responses:
        400:
          description: Bad request
          content: {}
        200:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UserSchema"
  /auth/user:
    get:
      tags:
        - "Учетная запись"
      summary: Информация о текущем пользователе
      security:
        - bearerAuth: []
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UserStoreSchema"
        401:
          description: Unauthenticated
  /auth/password/forgot:
    get:
      tags:
        - "Учетная запись"
      operationId: POST_auth-password-forgot
      summary: Забыли пароль
      description: |-
        Мы отправляем токен для сброса пароля после отправки письма.
        Для этого пользователю необходимо проверить свою электронную почту и перейти по ссылке на фронте.
      requestBody:
        description: параметры
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/ResetPasswordSchema"
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    default: Письмо успешно отправлено. Пожалуйста, проверьте свою электронную почту!
        401:
          description: Unauthenticated
  /auth/password/change:
    get:
      tags:
        - "Учетная запись"
      operationId: PATCH_auth-password-change
      summary: Изменение пароля
      description: |-
        Пользователь имеет возможность изменить свой пароль, указав свой текущий пароль и новый пароль.
      security:
        - bearerAuth: []
      requestBody:
        description: параметры
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/ChangePasswordSchema"
      responses:
        204:
          description: No content
          content: {}
        401:
          description: Unauthenticated
  /auth/user/token:
    get:
      tags:
        - "Учетная запись"
      operationId: GET_auth-user-token
      summary: Получить токен пользователя
      security:
        - bearerAuth: []
      responses:
        200:
          description: successful response
          content:
            application/json:
              schema:
                type: object
                properties:
                  device:
                    type: string
                    description: имя устройства
                  last_used_at:
                    type: string
                    description: дата последнего использования
                  expire_at:
                    type: string
                    description: Дата истечения срока
                  meta:
                    type: object
                    $ref: "#/components/schemas/UserSchema"
        401:
          description: Unauthenticated
  /auth/user/token/refresh:
    get:
      tags:
        - "Учетная запись"
      operationId: GET_auth-user-token-refresh
      summary: Обновить токен пользователя
      security:
        - bearerAuth: []
      responses:
        200:
          description: successful response
          content:
            application/json:
              schema:
                type: object
                properties:
                  access_token:
                    type: string
                    description: Токен
                  expire_at:
                    type: string
                    description: Дата истечения срока
        401:
          description: Unauthenticated

  /brands:
    get:
      tags:
        - Бренды
      operationId: GET_brands
      summary: Список брандов
      security:
        - bearerAuth: []
      parameters:
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/BrandSchema"
        401:
          description: Unauthenticated
  /brands/search:
    get:
      tags:
        - Бренды
      operationId: GET_brands
      summary: Список брандов
      security:
        - bearerAuth: []
      parameters:
        - name: q
          in: query
          description: "параметр поиска: может быть именем или описанием"
          required: true
          schema:
            type: string
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/BrandSchema"
        401:
          description: Unauthenticated

  /categories:
    get:
      tags:
        - Категории
      operationId: GET_categories
      summary: Список категории
      description: |-
        #### parameters
        - `id`: идентификатор категории
        - `name`: название категории
        - `parent_id`: родительский идентификатор категории
        - `children`: категория дети
        - `properties`: категория свойств по группам

        #### Queries parameters
        - `with`: отображать связанные поля
         <div>
         <b>children</b>: список категорий с детьми<br/>
         <b>props</b>: список категорий с характеристиками
        </div>
         
         пример: `/categories/?with=children,props`

        #### Пагинация
        - `page` : текушая страница
        - `per_page`  :   Количество строк по странице: default: 25
      security:
        - bearerAuth: []
      parameters:
        - name: with
          in: query
          description: |-
            #### отображать связанные поля
            <div>
            <b>children</b>: список категорий с детьми<br/>
            <b>props</b>: список категорий с характеристиками
            </div>
          required: false
          schema:
            type: integer
            default: 1
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/CategoryListSchema"
        401:
          description: Unauthenticated
  /categories/search:
    get:
      tags:
        - Категории
      operationId: GET_categories-search
      summary: Поиск категории
      security:
        - bearerAuth: []
      parameters:
        - name: q
          in: query
          description: "параметр поиска: может быть именем или описанием"
          required: true
          schema:
            type: string
        - name: with
          in: query
          required: false
          schema:
            type: string
            default: "children,props"
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/CategoryListSchema"
        401:
          $ref: "#/components/responses/UnauthorizedError"

  /products:
    get:
      tags:
        - Товары
      summary: Список товаров
      security:
        - bearerAuth: []
      parameters:
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ProductSchema"
        401:
          $ref: "#/components/responses/UnauthorizedError"
  /products/stats:
    get:
      tags:
        - Товары
      summary: Статистика товаров по статусу
      security:
        - bearerAuth: []
      description: |-
        ##### Список статусов товаров
        <table>
            <thead>
                <tr>
                    <th>Описание </th>
                    <th>Значение</th>
                </tr>
            </thead>
            <tbody>
                <tr><td>Все</td><td>all</td></tr>
                <tr><td>С ошибками</td><td>wrong</td></tr>
                <tr><td>Проверка</td><td>checking</td></tr>
                <tr><td>Скрытые</td><td>hidden</td></tr>
                <tr><td>Архив</td><td>archived</td></tr>
                <tr><td>Черновик</td><td>draft</td></tr>
                <tr><td>Перемодерация</td><td>remoderation</td></tr>
            </tbody>
        </table>
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ProductStatSchema"
        401:
          $ref: "#/components/responses/UnauthorizedError"
  /products/search:
    get:
      tags:
        - Товары
      summary: Поиск товаров
      security:
        - bearerAuth: []
      description: |-
        ##### Таблица статусов для поиска
          <table>
              <thead>
                  <tr>
                      <th>Описание </th>
                      <th>Значение</th>
                  </tr>
              </thead>
              <tbody>
                  <tr><td>На проверке</td><td>checking</td></tr>
                  <tr><td>С ошибками</td><td>wrong</td></tr>
                  <tr><td>Активен</td><td>activated</td></tr>
                  <tr><td>В черновике</td><td>draft</td></tr>
                  <tr><td>В архиве</td><td>archived</td></tr>
                  <tr><td>На складе</td><td>available</td></tr>
                  <tr><td>На складе</td><td>unavailable</td></tr>
                  <tr><td>Премодерация</td><td>premoderation</td></tr>
              </tbody>
          </table>
      parameters:
        - name: q
          in: query
          description: |-
            поиск  по всем полями товаров, `название`, `артикул`, и тд.<br/> 
            **пример** : `?q=Iphone`
          required: false
          schema:
            type: string
        - name: state
          in: query
          description: |-
            фильтрация по статусами.<br/>
            **пример**: `?state=hidden`"
          required: false
          schema:
            type: string
        - name: categories
          in: query
          description: |-
            фильтрация по категориям по идентификатором<br/> 
            **пример** : `?categories=0000155,000145545` позволяет получать товары категории 0000155 и 000145545
          required: false
          schema:
            type: string
        - name: sort
          in: query
          description: |-
            Сортировка по названию: `name`,
            артикулу: `slug`,
            цене: `price`,
            остаткам: `quantity`<br/> 
            **пример** : `?sort=name`
          required: false
          schema:
            type: string
        - name: order
          in: query
          description: |-
            он используется с `sort` для отсортированные элементы по порядку <br/>
            **пример**: `?sort=price&order=desc` позволяет отсортировать товары по цене в порядке убывания
          schema:
            type: string
            enum:
              - asc
              - desc
            default: asc
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ProductSchema"
        401:
          $ref: "#/components/responses/UnauthorizedError"
  /products/{id}:
    get:
      tags:
        - "Товары"
      summary: Детали товар по id
      security:
        - bearerAuth: []
      parameters:
        - name: id
          in: path
          description: ID товара
          required: true
          schema:
            type: string
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                $ref: "#/components/schemas/ProductSchema"
        401:
          $ref: "#/components/responses/UnauthorizedError"
    put:
      tags:
        - "Товары"
      summary: Польное изменение товарa
      security:
        - bearerAuth: []
      requestBody:
        description: Pet object that needs to be added to the store
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/ProductPutSchema"
        required: true

      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                $ref: "#/components/schemas/ProductSchema"
        401:
          $ref: "#/components/responses/UnauthorizedError"
        400:
          description: Указан неверный идентификатор
        404:
          description: товар не найден
        422:
          description: Исключение проверки

  /stores/orders:
    get:
      tags:
        - "Заказы - Расчеты"
      summary: Список заказов
      security:
        - bearerAuth: []
      parameters:
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/OrderSchema"
        401:
          description: Unauthenticated
  /stores/orders/search:
    get:
      tags:
        - "Заказы - Расчеты"
      summary: Поиск и фильтрация заказов
      description: |-
        <table>
        <thead>
         <th>параметр</th>
         <th>примеры / значение</th>
        </thead>
        <tbody>
        <tr>
         <td>state</td>
         <td>sale/return</td>
        </tr>
        <tr>
         <td>products</td><td>00000000175,00000000187</td>
        </tr>
        <tr>
         <td>date</td>
         <td>YYYY-MM-DD HH:MM:SS</td>
        </tr>
        </tbody>
        </table>

      security:
        - bearerAuth: []
      parameters:
        - name: date
          in: query
          description: Дата заказа
          required: false
          schema:
            type: string
        - name: state
          in: query
          description: Статус заказа
          required: false
          schema:
            type: string
            enum:
              - sale
              - return
        - name: products
          in: query
          description: Список ID товаров заказа
          schema:
            type: array
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/OrderSchema"
        401:
          description: Unauthenticated
  /stores/orders/{id}:
    get:
      tags:
        - "Заказы - Расчеты"
      summary: Заказ по номерy или по id
      security:
        - bearerAuth: []
      parameters:
        - name: id
          in: path
          description: ID или Номер заказа
          required: true
          schema:
            type: string
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                $ref: "#/components/schemas/OrderSchema"
        401:
          description: Unauthenticated
  /stores/accounting:
    get:
      tags:
        - "Заказы - Расчеты"
      summary: Список Расчеты
      security:
        - bearerAuth: []
      parameters:
        - name: page
          in: query
          description: "**Пагинация**: текущая страница"
          required: false
          schema:
            type: integer
            default: 1
        - name: per_page
          in: query
          description: "**Пагинация**: строк на странице"
          required: false
          schema:
            type: integer
            default: -1
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/AmountSchema"
        401:
          description: Unauthenticated

components:
  securitySchemes:
    bearerAuth: # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: Token # optional, arbitrary value for documentation purposes
      description: Аутентификация
  responses:
    UnauthorizedError:
      description: Токен доступа отсутствует или недействителен
  schemas:
    UserSchema:
      properties:
        id:
          type: integer
          format: int64
          description: User Id
        name:
          type: string
        phone:
          type: string
          description: Телефон пользователя
        is_active:
          type: boolean
        access_token:
          type: string
          description: Токен
        expire_at:
          type: string
          description: Срок действия токена
        created_at:
          type: string
          pattern: YYYY-MM-DD HH:I:S
        updated_at:
          type: string
          pattern: YYYY-MM-DD
      type: object
      xml:
        name: User

    UserStoreSchema:
      allOf:
        - $ref: "#/components/schemas/UserSchema"
        - type: object
          properties:
            store:
              description: User store
              $ref: "#/components/schemas/StoreSchema"

    LoginSchema:
      required:
        - email
        - password
        - device_name
      properties:
        email:
          type: string
          description: Email пользователя
        password:
          type: string
          description: Пароль  пользователя
        device_name:
          type: string
          description: Платформа пользователя
      type: object
      xml:
        name: LoginSchema

    RegisterSchema:
      required:
        - name
        - email
        - password
      properties:
        name:
          type: string
          description: Имя  пользователя
        email:
          type: string
          description: Email пользователя
        phone:
          type: string
          description: Телефон пользователя
        password:
          type: string
          description: Пароль  пользователя
      type: object
      xml:
        name: RegisterSchema

    ResetPasswordSchema:
      required:
        - email
      properties:
        email:
          type: string
          description: Email пользователя
      type: object
      xml:
        name: ResetPasswordSchema

    ChangePasswordSchema:
      required:
        - old_password
        - password
        - password_confirm
      properties:
        old_password:
          type: string
          description: Текущий пароль пользователя
        password:
          type: string
          description: Новый пароль пользователя
        password_confirm:
          type: string
          description: Подтвердите пароль пользователя
      type: object
      xml:
        name: ChangePasswordSchema

    StoreSchema:
      properties:
        id:
          type: string
        name:
          type: string
          description: Название магазина
        contact:
          type: string
        website:
          type: string
        address:
          type: string
      type: object
      xml:
        name: Store

    BrandSchema:
      properties:
        id:
          type: string
        name:
          type: string
          description: Название бренда
        description:
          type: string
          description: Описание бренда
      type: object
      xml:
        name: Brand

    CategorySchema:
      properties:
        id:
          type: string
          description: идентификатор категории
        name:
          type: string
          description: название категории
        parent_id:
          type: string
          description: родительский идентификатор категории
      type: object
      xml:
        name: Category

    CategoryListSchema:
      properties:
        id:
          type: string
          description: идентификатор категории
        name:
          type: string
          description: название категории
        parent_id:
          type: string
          description: родительский идентификатор категории
        children:
          type: array
          description: категория дети
          items:
            $ref: "#/components/schemas/CategorySchema"
        properties:
          type: array
          description: категория свойств по группам
          items:
            $ref: "#/components/schemas/CategoryPropertySchema"
      type: object
      xml:
        name: Category

    CategoryPropertySchema:
      properties:
        group:
          type: string
          description: группа характеристики
        list:
          type: array
          description: характеристики
          items:
            $ref: "#/components/schemas/PropertySchema"

    PropertySchema:
      properties:
        id:
          type: string
          description: идентификатор характеристики
        name:
          type: string
          description: название характеристики
        type:
          type: string
          description: тип характеристики
        description:
          type: string
          description: Описание характеристики
        values:
          type: array
          description: значения характеристики
          items:
            $ref: "#/components/schemas/PropertyValueSchema"

    PropertyValueSchema:
      properties:
        id:
          type: string
          description: идентификатор значение
        value:
          type: string
          description: название значение
        # description:
        #   type: string
        #   description: Описание значение

    OrderSchema:
      properties:
        id:
          type: integer
          description: ID заказа
        number:
          type: string
          description: Номер заказа
        status:
          type: object
          description: Статус заказа
          $ref: "#/components/schemas/StatusSchema"
        date:
          type: string
          description: Дата заказа
        amount:
          type: number
          description: Сумма заказа
        product_count:
          type: integer
          description: Кол-во товаров заказа
        products:
          type: array
          description: Список товаров заказа
          items:
            $ref: "#/components/schemas/OrderProductSchema"

    OrderProductSchema:
      properties:
        id:
          type: integer
          description: ID товаров
        name:
          type: string
          description: Название товара
        price:
          type: number
          description: Цена товара
        quantity:
          type: integer
          description: остаток товара
        photo:
          type: string
          description: Главное фото товара

    AmountSchema:
      properties:
        id:
          type: integer
          description: ID Расчета
        number:
          type: string
          description: Номер Расчета
        date:
          type: string
          description: Дата Расчета
        amount:
          type: number
          description: Сумма Расчета

    StatusSchema:
      properties:
        id:
          type: integer
        title:
          type: string
        name:
          type: string
      type: object
      xml:
        name: Status

    Product:
      properties:
        id:
          type: integer
          description: ID товаров
          readOnly: true
        name:
          type: string
          description: Название товара
        description:
          type: string
          description: Описние товара
        slug:
          type: string
          description: Артикул товара
        barcode:
          type: string
          description: штрифкод товара
        price:
          type: number
          description: Цена товара
        weight:
          type: number
          description: Вес товара
        length:
          type: number
          description: длина товара
        height:
          type: number
          description: высота товара
        width:
          type: number
          description: ширина товара
        vat:
          type: number
          description: НДС товара
        isvisible:
          type: boolean
          description: скрытый или нет товар
          readOnly: true
        archived:
          type: boolean
          description: в архиве или нет
          readOnly: true
        quantity:
          type: integer
          description: остаток товара
        imported:
          type: integer
          description: товар, импортированный из файла yml
          readOnly: true

    ProductForeign:
      properties:
        brand_id:
          type: integer
          description: ID бранд товара
        category_id:
          type: integer
          description: ID  категории товара
        store_id:
          type: integer
          description: ID  партнера
          readOnly: true
        status_id:
          type: integer
          description: ID  статуса товара
          readOnly: true

    ProductPostSchema:
      required:
        - "name"
        - "slug"
        - "weight"
        - "length"
        - "height"
      allOf:
        - $ref: "#/components/schemas/Product"
        - $ref: "#/components/schemas/ProductForeign"
        - type: object

    ProductPutSchema:
      allOf:
        - $ref: "#/components/schemas/Product"
        - $ref: "#/components/schemas/ProductForeign"
        - type: object
        - properties:
            images:
              type: array
              description: список изображений
              example:
                - 12
                - 45
            properties:
              type: array
              description: список характеристики

    ProductPatchSchema:
      allOf:
        - $ref: "#/components/schemas/Product"
        - $ref: "#/components/schemas/ProductForeign"
        - type: object
        - properties:
            state:
              type: string
              description: sfjsdjfjs

    ProductSchema:
      allOf:
        - $ref: "#/components/schemas/Product"
        - type: object
          properties:
            brand:
              type: object
              description: ID бранд товара
              $ref: "#/components/schemas/BrandSchema"
            category:
              type: integer
              description: ID  категории товара
              $ref: "#/components/schemas/CategorySchema"
            status:
              type: integer
              description: ID  статуса товара
              $ref: "#/components/schemas/StatusSchema"
            images:
              type: array
              description: список изображений товара
              items:
                $ref: "#/components/schemas/ImageSchema"
            properties:
              type: array
              description: список характеристики товара
              items:
                $ref: "#/components/schemas/ProductPropertySchema"

    ImageSchema:
      properties:
        id:
          type: integer
        name:
          type: string
          description: Название картины
        url:
          type: string
          description: Ссылка картины
        is_main:
          type: boolean
          description: Главная картины
        tabindex:
          type: string
          description: Позиция картины

    ProductPropertySchema:
      allOf:
        - $ref: "#/components/schemas/PropertyValueSchema"
        - type: object
          properties:
            is_value_id:
              type: boolean
              description: Проверить, является ли значение id значения свойства

    ProductStatSchema:
      properties:
        name:
          type: string
          description: назвоние статус
        value:
          type: string
          description: значение
        productCount:
          type: integer
          description: количество товаров по каждому статусу
