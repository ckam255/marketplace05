<?php
function validate_images()
{
    if (request()->hasFile('images')) {
        $images = request()->file('images');
        $extentions = ['png', 'jpg', 'jpeg', 'webp'];

        foreach ($images as $image) {
            $ext = $image->getClientOriginalExtension();
            if (!in_array($ext, $extentions)) {
                return ['images' => "Формат *.{$ext} запрещен"];
            }
        }
    }
}

function properties_validator($properties)
{
    if (!is_array($properties)) {
        return ['свойства' => 'поле [properties] должно быть массивом объектов.'];
    }
    foreach ($properties as $property) {
        if (empty($property['name'])) {
            return  ['свойства' => 'поле [name] обязательныи.'];
        }
        if (empty($property['type'])) {
            return  ['свойства' => 'поле [type] обязательныи.'];
        }
        if (!empty($property['multiple']) && !is_bool($property['multiple'])) {
            return  ['свойства' => 'поле [type] должно быть логическим.'];
        }

        if (empty($property['values'])) {
            return  ['свойства' => 'добавить значения  свойств(-- поле [values])'];
        } else {
            $values = $property['values'];
            if (!is_array($values)) {
                return ['свойства' => 'поле [values] должно быть массивом объектов.'];
            }
            foreach ($values as $value) {
                if (empty($value['value'])) {
                    return  ['свойства' => 'поле [value] обязательныи.'];
                }
            }
        }
    }
}

function validate_propuct_characteristics()
{
    if (request()->exists('characteristics')) {
        $characteristics = request()->get('characteristics');
        if (!is_array($characteristics)) {
            return ['характеристики' => 'поле [characteristics] должно быть массивом объектов.'];
        }
        foreach ($characteristics as $charac) {
            if (empty($charac['name'])) {
                return  ['характеристики' => 'поле [name] обязательныи.'];
            }
            if (!empty($charac['properties'])) {
                $properties = $charac['properties'];
                return properties_validator($properties);
            }
        }
        return null;
    }
}

function validate_propuct_properties()
{
    if (request()->exists('properties')) {
        $properties = request()->get('properties');
        return properties_validator($properties);
    }
}

function validate_propuct_status()
{
    if (request()->exists('status')) {
        $status = request()->get('status');
        // if (empty($status['code'])) {
        //     return  ['статус' => 'поле [code] обязательныи.'];
        // } else  
        if (!is_int($status)) {
            return ['статус' => 'поле [status] должен быть целым числом'];
        }
        return null;
    }
}

function validate_propuct_stock()
{
    if (request()->exists('stock')) {
        $stock = (int)request()->get('stock');
        if (empty($stock)) {
            return  ['статус' => 'поле [stock] не может быть пустым.'];
        } else  if (!is_integer($stock)) {
            return ['склад' => 'поле [stock] должен быть целым числом'];
        }
    }
}


function validate_product_group_properties()
{
    if (request()->exists('properties')) {

        $properties = request()->get('properties');
        if (!is_array($properties)) {
            return ['свойства' => 'поле [properties] должно быть массивом объектов.'];
        }
        foreach ($properties as $prop) {
            if (empty($prop['value'])) {
                return  ['свойства' => 'поле [value] обязательныи.'];
            }
            if (empty($prop['property_id'])) {
                return  ['свойства' => 'поле [property_id] обязательныи.'];
            }
            // if (!is_int($prop['property_id'])) {
            //     return ['статус' => 'поле [property_id] должен быть целым числом'];
            // }
            if (empty($prop['group_id'])) {
                return  ['свойства' => 'поле [group_id] обязательныи.'];
            }
            // if (!is_int($prop['group_id'])) {
            //     return ['статус' => 'поле [group_id] должен быть целым числом'];
            // }
        }
    }
}
