<?php


function exists($node, array $response)
{
    return !empty($response[$node]);
}

function ucode()
{
    usleep(10);
    return abs(crc32(uniqid()));
}

function uhex()
{
    usleep(10);
    return hexdec(uniqid());
}

function ucrc()
{
    usleep(10);
    return abs(crc32(uniqid()));
}



function months()
{
    return array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
}

function days()
{
    return   array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
}

function month($date)
{
    return months()[parseDateInt($date, 'n')];
}

function day($date)
{
    // $day = is_int($date) ? $date : date('N', strtotime($date)) - 1;
    return days()[parseDateInt($date)];
}

function parseDateInt($val, $pref = 'N')
{
    return is_int($val) ? $val : date($pref, strtotime($val)) - 1;
}


function removeObjectKeys($object, array $keys)
{
    $arr = (array)$object;
    foreach ($keys as $k) {
        unset($arr[$k]);
    }
    return $arr;
}

function toArray($object): array
{
    $output = [];
    foreach ($object as $obj) {
        $output[] = $obj;
    }
    return $output;
}

function is_image_valid($image)
{
    $extentions = ['png', 'jpg', 'webp'];
    $ext = $image->getClientOriginalExtension();
    return in_array($ext, $extentions);
}

function states(string $state)
{
    switch ($state) {
        case 'hidden': // Скрытен
            return 0;
        case 'visible': // Не Скрытен
            return 1;
        case 'draft': // В черновике
            return 5;
        case 'checking': // На проверке
            return 1;
        case 'wrong': // С ошибками
            return 2;
        case 'remoderation': // Перемодерация
            return 3;
        case 'activated': // Активен
            return 4;
        case 'archived': // В архиве
            return 1;
        case 'unarchived': // В архиве
            return 0;
        case 'rejected': // Неактивен
            return 14;
        case 'sale': // Неактивен
            return 20;
        case 'return':
            return 21;
        default:
            return 1;
    }
}
function in_status($status)
{
    return in_array($status, [1, 2, 3, 4, 5, 6, 11, 12, 13, 14, 15]);
}
function in_status_text($status)
{
    return in_array($status, ['hidden', 'visible',  'checking', 'wrong', 'archived', 'rejected']); // 'remoderation', 'draft',
}

function product_filter_params()
{
    return ['hidden', 'visible', 'unavailable', 'available',  'checking', 'wrong',  'archived']; // 'remoderation', 'draft',
}

function is_product_filter_param(string $q)
{
    return in_array($q, product_filter_params());
}

function is_product_order_param(string $q)
{
    return in_array($q, ['name', 'slug', 'price', 'quantity', 'date']);
}

function file_gen_name($prefix = '', string $ext = 'txt')
{
    return $prefix . '' . date('dmYHis') . '.' . $ext;
}

function export($data, $path)
{
    $abs_path  = storage_path($path);
    file_put_contents($abs_path, $data);
    return $abs_path;
}

function get_order_status(bool $type)
{
    return $type ? 'Возврат' : 'Продажа';
}

function fetch_dev_data()
{
    $url = "http://185.247.119.145/1c/marketplace2.json";
    set_time_limit(-1);
    $file = file_get_contents($url);
    $path = date('dmY') . '.json';
    $path = storage_path("sync/{$path}");
    if (file_exists($path)) {
        unlink($path);
    }
    file_put_contents($path, $file);
    return $path;
}


function image_path($filename)
{
    if (env('APP_ENV') === 'local') {
        return url('api/images/' . $filename);
    }
    return env('APP_URL') . "/api/images/$filename";
}

function document_path($filename)
{
    if (env('APP_ENV') === 'local') {
        return url('api/documents/' . $filename);
    }
    return env('APP_URL') . "/api/documents/$filename";
}
