<?php

namespace App\Console;

use App\Jobs\RemoveUselessImageJob;
use App\Repositories\ImageRepository;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        sleep(60);
        $schedule->command('sync:clear')->weekly()->sundays()->at('02:00')->runInBackground();
        sleep(60);
        $schedule->job(new RemoveUselessImageJob())->weekly()->sundays()->at('04:00')->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
