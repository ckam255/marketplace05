<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user {name} {email} {password} {--role= : role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $role = strtolower($this->option('role'));
            $data = [
                'name' => $this->argument('name'),
                'email' => $this->argument('email'),
                'password' => Hash::make($this->argument('password')),
                'is_superuser' => $role === 'superuser',
                'is_admin' => $role === 'admin',
                'is_active' => true,
            ];
            \App\Models\User::create($data);
            return  $this->info("User successfully created");
        } catch (\Exception $ex) {
            $this->error(printf('%s already exists ', $data['email']));
        }

        return 0;
    }
}
