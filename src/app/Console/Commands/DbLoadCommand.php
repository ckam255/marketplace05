<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use App\Repositories\SyncDataRepository;

class DbLoadCommand extends Command
{
    private SyncDataRepository $repository;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:load {--t|target= : specify node to load} {--f|file= : local filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load data from json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SyncDataRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {
            // load status
            $c = \App\Models\Status::count();
            if ($c === 0) {
                Artisan::call('db:seed --class=StatusSeeder');
            }

            if (!$file = $this->option('file')) {
                $filename = date('dmY') . ".json";
                $path = storage_path("sync/{$filename}");
                if (!file_exists($path)) {
                    $this->info(printf('Fetching data...' . PHP_EOL));
                    $path = fetch_dev_data();
                    $this->info(printf('File successfully downloaded: %s' . PHP_EOL, $path));
                }
            } else {
                $path = storage_path("sync/{$file}");
            }

            $name = $this->option('target');
            $msg = '';
            switch ($name) {
                case 'brands':
                    $msg =  $this->repository->syncBrands($path);
                    break;
                case 'properties':
                    $msg =  $this->repository->syncProperties($path);
                    break;
                case 'categories':
                    $msg =  $this->repository->syncCategories($path);
                    break;
                case 'tariffs':
                    $msg =  $this->repository->syncTariffs($path);
                    break;
                case 'stores':
                    $msg =  $this->repository->syncStores($path);
                    break;
                case 'products':
                    $msg =  $this->repository->syncProducts($path);
                    break;
                case 'orders':
                    $msg =  $this->repository->syncOrders($path);
                    break;
                default:
                    $this->repository->syncAll($path);
                    $msg = 'loading all data...';
                    break;
            }
            $this->info(printf($msg . PHP_EOL));
        } catch (\Exception $ex) {
            $this->error(printf("Error: %s" . PHP_EOL, $ex->getMessage()));
        }

        return 0;
    }
}
