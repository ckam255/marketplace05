<?php

namespace App\Console\Commands;

use App\Services\DatabaseService;
use Illuminate\Console\Command;

class DbRestoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:restore {path} {--t|type= : specify file type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore database from file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rep = DatabaseService::restore($this->argument('path'), $this->option('type'));
        $this->info($rep);
    }
}
