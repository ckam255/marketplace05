<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CleanLogsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:clear {--l|log= : clear specify file log}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all logs ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Logs may be needed in case of problems. continue?')) {
            $dir = storage_path("logs");
            if ($this->option('log')) {
                $log = "$dir/" . $this->option('log') . ".log";
                if (!file_exists($log)) {
                    return $this->error(sprintf('%s does not exists', $log));
                }
                exec("truncate -s 0 $log");
                $this->info(sprintf('%s Successfully cleaned ', $log));
            } else {
                $files = scandir($dir);
                foreach ($files as $file) {
                    if (!str_starts_with($file, '.')) {
                        exec("truncate -s 0 $dir/$file");
                        $this->info(printf("%s successfully cleaned" . PHP_EOL, $file));
                    }
                }
            }
        }
        return 0;
    }
}
