<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class CleanSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear sync directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Are you sur to execute this command?')) {
            $todayFile = date('dmY') . '.json';
            $syncDir = storage_path() . "/sync";
            $files = scandir($syncDir);
            foreach ($files as $file) {
                if (!str_starts_with($file, '.') && $file !== $todayFile && $file !== 'example.json') {
                    unlink("$syncDir/$file");
                    $this->info(printf("%s removed" . PHP_EOL, $file));
                }
            }
        }
        return 0;
    }
}
