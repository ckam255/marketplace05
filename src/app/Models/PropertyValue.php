<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden = [
        "property_id",
        "created_at",
        "updated_at"
    ];
}
