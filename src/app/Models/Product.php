<?php

namespace App\Models;

use App\Services\Paginator;
use App\Services\ProductFilterService;
use App\Traits\HasExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends BaseModel
{
    use HasFactory;
    use HasExport;

    protected $guarded = [];

    protected $hidden = [
        'exceptions'
    ];

    protected $casts = [
        'price' => 'float',
        'weight' => 'float',
        'length' => 'float',
        'height' => 'float',
        'width' => 'float',
    ];

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'product_property')->withPivot(['value', 'is_value_id']);
    }

    public function productable()
    {
        return $this->morphTo();
    }

    public function images()
    {
        return $this->hasMany(Image::class)->orderBy('tabindex');
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function isReadonly()
    {
        return $this->imported === 'yaml' || $this->imported === 'yml';
    }

    public function list()
    {
        return $this->paginate(Store::auth()->products())
            ->get()->map->toArray();
    }

    public function archived()
    {
        return  ProductFilterService::current()->archived();
    }

    public function getSingleOrFail($id)
    {
        $product = Store::auth()->products()
            ->where('id', $id)->first();
        if (!$product) {
            abort(404, 'Товар не найден');
        }
        return $product;
    }

    public function getById($id)
    {
        // return Store::auth()->products()->count();
        $query = Store::auth()->products()
            ->where('id', $id);

        if (!$product = $query->first()) {
            return null;
        }
        return $product->toArray(true);
    }

    public function getInList(array $ids)
    {
        return Store::auth()->products()
            ->whereIn('id', $ids)->get();
    }

    public function search()
    {
        $query = Store::auth()->products();
        $search =  request()->input('q');
        $searchCount = 0;

        if ($search != "") {
            $searchCount += 1;
            $query = $this->searchScope($query, $search);
        }
        return   $this->filterScope($query, $searchCount);
    }

    public function filter()
    {
        if (!request()->exists('state') && !request()->exists('categories')) {
            abort(400, 'пожалуйста, введите что вы хотите искать');
        }

        if (request()->exists('categories')) {
            $categories = explode(',', trim(request()->get('categories')) ?? '');
            $query = ProductFilterService::current()->byCategories($categories);
            return $this->paginate($query);
        }

        if (request()->exists('state')) {

            $state =  trim(request()->get('state'));
            if (!is_product_filter_param($state)) {
                abort('400', 'Параметр не разрешен');
            }

            // Скрытен
            if ($state === 'hidden') {
                $query = ProductFilterService::current()->hidden();
                return $this->paginate($query);
            }
            // Не Скрытен
            if ($state === 'visible') {
                $query = ProductFilterService::current()->visible();
                return $this->paginate($query);
            }

            // На складе
            if ($state === 'available') {
                $query = ProductFilterService::current()->available();
                return $this->paginate($query);
            }

            // Нет на складе
            if ($state === 'unavailable') {
                $query = ProductFilterService::current()->unavailable();
                return $this->paginate($query);
            }
            // На проверке
            if ($state === 'checking') {
                $query = ProductFilterService::current()->checking();
                return $this->paginate($query);
            }
            // С ошибками
            if ($state === 'wrong') {
                $query = ProductFilterService::current()->wrong();
                return $this->paginate($query);
            }
            // // Перемодерация
            // if ($state === 'remoderation') {
            //     $query = ProductFilterService::current()->remoderation();
            //     return $this->paginate($query);
            // }
            // // В черновике
            // if ($state == 'draft') {
            //     $query = ProductFilterService::current()->draft();
            //     return $this->paginate($query);
            // }
            // В архиве
            if ($state === 'archived') {
                $query = ProductFilterService::current()->archived();
                return $this->paginate($query);
            }
        }
    }

    public function filterScope($query, $searchCount = 0)
    {

        if ($searchCount === 0 && !request()->exists('state') && !request()->exists('categories')) {
            abort(400, 'пожалуйста, введите что вы хотите искать');
        }

        if (request()->exists('categories')) {
            $categories = explode(',', trim(request()->get('categories')) ?? '');
            $query = ProductFilterService::current($query)->byCategories($categories);
        }

        if (request()->exists('state')) {

            $state = explode(',', trim(request()->get('state')) ?? '');
            foreach ($state as $ste) {
                if (!is_product_filter_param($ste)) {
                    abort('400', 'Параметр не разрешен');
                }
            }
            // Скрытен
            if (in_array('hidden', $state)) {
                $query = ProductFilterService::current($query)->hidden();
            }
            // Не Скрытен
            if (in_array('visible', $state)) {
                $query = ProductFilterService::current($query)->visible();
            }
            // На складе
            if (in_array('available', $state)) {
                $query = ProductFilterService::current($query)->available();
            }

            // Нет на складе
            if (in_array('unavailable', $state)) {
                $query = ProductFilterService::current($query)->unavailable();
            }
            // На проверке
            if (in_array('checking', $state)) {
                $query = ProductFilterService::current($query)->checking();
            }
            // С ошибками
            if (in_array('wrong', $state)) {
                $query = ProductFilterService::current($query)->wrong();
            }
            // Перемодерация
            if (in_array('remoderation', $state)) {
                $query = ProductFilterService::current($query)->remoderation();
            }
            // В черновике
            if (in_array('draft', $state)) {
                $query = ProductFilterService::current($query)->draft();
            }
            // В архиве
            if (in_array('archived', $state)) {
                $query = ProductFilterService::current($query)->archived();
            }
        }

        return $this->paginate($query);
    }

    public function searchScope($query, $search)
    {
        return $query->where(function ($q) use ($search) {
            $q->where('name', 'ILIKE', '%' . $search . '%')
                ->orWhere('slug', 'ILIKE', '%' . $search . '%')
                ->orWhere('barcode', 'ILIKE', '%' . $search . '%')
                ->orWhere('description', 'ILIKE', '%' . $search . '%')
                ->orWhere('price', 'ILIKE', '%' . $search . '%')
                ->orWhere('weight', 'ILIKE', '%' . $search . '%')
                ->orWhere('length', 'ILIKE', '%' . $search . '%')
                ->orWhere('height', 'ILIKE', '%' . $search . '%')
                ->orWhere('width', 'ILIKE', '%' . $search . '%')
                ->orWhere('vat', 'ILIKE', '%' . $search . '%')
                ->orWhere('length', 'ILIKE', '%' . $search . '%');
        });
    }

    public function commission()
    {
        if ($this->category !== null) {
            $this->category->tariff;
        }
        return  $this->store->tariff;
    }

    public function paginate($query)
    {
        return $this->withPagination($this->sort($query));
    }

    public function sort($query)
    {
        if (request()->exists('sort')) {
            $field = strtolower(request()->get('sort'));
            $ordered = request()->get('order') ?? 'asc';
            if (!is_product_order_param($field)) {
                abort('400', "Параметр {$field}  не разрешен");
            }
            if (!in_array(strtolower($ordered), ['asc', 'desc'])) {
                abort('400', "Значение order должно быть ASC или DESC");
            }
            if ($field == 'date') {
                $field = 'created_at';
            }
            $query->orderBy($field, $ordered);
        }
        return $query;
    }

    public function toArray(bool $props = true)
    {
        $array = parent::toArray();
        $array['category'] =  $this->category;
        $array['brand'] =  $this->brand;
        $array['status'] =  $this->status()->latest('updated_at')->first();
        $array['status']['comment'] = $this->exceptions;
        if ($props) {
            $array['images'] =  $this->images()->get();
            $array['properties'] = $this->properties()->get()->map->asProp();
        } else {
            $array['images'] =  $this->images()->get()->take(10);
        }
        $array['tariff'] =  $this->commission();
        unset($array['brand_id']);
        unset($array['status_id']);
        unset($array['category_id']);
        return $array;
    }

    public function exportable()
    {
        $array = parent::toArray();
        $array['store_id'] =  $this->store->code_1c ?? null;
        $array['brand_id'] =  $this->brand->code_1c ?? null;
        $array['category_id'] =  $this->category->code_1c ?? null;
        $array['images'] =  $this->images()->get();
        $array['properties'] =  $this->properties()->with('values')->get()->map->export();
        return $array;
    }
}
