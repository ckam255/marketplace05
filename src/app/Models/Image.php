<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $guarded = [];


    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    public function toArray()
    {
        $array = parent::toArray();
        // $array["id"] = $this->id;
        // $array["name"] =  $this->name;
        // $array["url"] = $this->url;
        return $array;
    }
}
