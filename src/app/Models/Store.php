<?php

namespace App\Models;

use App\Traits\HasExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Store extends BaseModel
{
    use HasFactory;
    use HasExport;

    protected $guarded = ['id'];
    protected $hidden = ['tariff_id', 'status_id'];

    public function current()
    {
        return $this->query()->where('user_id', Auth::user()->id)->first();
    }

    public static function auth()
    {
        if (!$store = Auth::user()->store) {
            abort(400, "У вас нет магазина");
        }
        return $store;
    }

    public function legal()
    {
        return $this->hasOne(Legal::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public static function authenticate()
    {
        return self::findOrFail(auth()->user()->id);
    }

    public function worktime()
    {
        return $this->hasOne(Worktime::class);
    }

    public function amounts()
    {
        return $this->hasMany(Amount::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);;
    }

    public function getTariffAttribute()
    {
        return $this->tariff()->first();
    }

    public function getStatusAttribute()
    {
        return $this->status()->first();
    }

    public function getDocumentsAttribute()
    {
        return $this->documents()->get();
    }

    public function getWorktimeAttribute()
    {
        return $this->worktime()->first();
    }

    public function getLegalAttribute()
    {
        return $this->legal()->first();
    }

    public function exportable()
    {
        $array = parent::toArray();
        $array['tariff_id'] =  $this->tariff->code_1c ?? null;
        $array['legal'] =  $this->legal;
        $array['documents'] =  $this->documents;
        return $array;
    }

    public function isModerable()
    {
        if ($this->legal && $this->documents->count() > 0) {
            return true;
        }
        return false;
    }
}
