<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tariff extends BaseModel
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden = [
        'pivot',
        'created_at'
    ];

    public function toArray()
    {
        return [
            'minimal_fee' => $this->min_commission_RUB,
            'cash_fee_percent' => $this->commission,
            'online_fee_percent' => $this->commission_acquiring
        ];
    }
}
