<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;


    protected $table = 'status';
    public $incrementing = false;

    protected $guarded = [];


    protected $hidden = [
        'statusable_id',
        'statusable_type',
        'created_at',
        'pivot',
        'updated_at'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function statusable()
    {
        return $this->morphTo();
    }

    public function format()
    {
        return $this->id;
    }

    public function single()
    {
        return $this->title;
    }
}
