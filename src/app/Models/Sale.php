<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function format()
    {
        $product =  $this->product; //()->get()->map(function ($prod) {
        $photo = $product->images()->where('is_main', true)->first();
        return [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $this->sum,
            'quantity' => $this->quantity,
            'photo' => $photo->url ?? null
        ];
    }
}
