<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Amount extends BaseModel
{
    use HasFactory;

    protected $guarded = [];

    public function toArray(...$except)
    {
        $data =  parent::toArray();
        foreach ($except as $exc) {
            unset($data[$exc]);
        }
        return $data;
    }
}
