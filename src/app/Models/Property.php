<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends BaseModel
{
    use HasFactory;

    protected $hidden = [
        'propertyable_id',
        'propertyable_type',
        // 'pivot',
        "created_at",
        "updated_at"
    ];


    protected $guarded = [];


    public function values()
    {
        return $this->hasMany(PropertyValue::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_property_values');
    }

    public function format()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'description' => $this->description,
            'values' => $this->values,
            'group' => $this->pivot->group_name
        ];
    }

    public function asProp()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_value_id' => $this->pivot->is_value_id,
            'value' =>  json_decode(($this->pivot->value))
        ];
    }


    public function export()
    {
        return [
            'id' => $this->code_1c,
            'is_value_id' => $this->pivot->is_value_id,
            //  'value' =>  $this->type === 'multiple' ? json_decode(($this->pivot->value)) : $this->pivot->value,
            'value' =>   json_decode(($this->pivot->value)),
        ];
    }
}
