<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden = ['type'];
    public $timestamps = false;

    public function status()
    {
        //return get_order_status($this->type);
        return $this->belongsTo(Status::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function format()
    {
        $sales = $this->sales()->get();
        return [
            'id' => $this->id,
            'number' => $this->number,
            'store_id' => $this->store_id,
            'status' => $this->status()->first(),
            'date' => $this->created_at,
            'amount' => $this->sum, //$this->amount($sales),
            'product_count' => $sales->count(),
            'products' => $sales->map->format(),
        ];
    }


    public function amount($sales)
    {
        return $sales->reduce(function ($carry, $sale) {
            return  $carry + ($sale->quantity * $sale->product->price);
        }, 0);
    }
}
