<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends BaseModel
{
    use HasFactory;

    protected $guarded = [];

    protected $hidden = [
        'created_by'
    ];

    public function categoryable()
    {
        return $this->morphTo();
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }

    public function paginate()
    {
        $query = $this->query()->Where('parent_id', null);
        return $this->withPagination($query);
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'category_property')->withPivot('group_name');
    }

    public function format(bool $children = false, bool $props = false)
    {
        $data =  [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            //'parent' => $this->parent
            'parent_id' => $this->parent_id
        ];
        if ($children) {
            $data['children'] = $this->withChildren();
        } else {
            $data['children'] = count($this->children) > 0 ?  true : false;
        }
        if ($props) {
            $data['properties'] = $this->withProps();
        }
        return $data;
    }

    public function withChildren()
    {
        return  $this->children()->with('children', function ($cat) {
            $cat->with('children');
        })->get();
    }

    public function withProps()
    {
        $pros =  $this->properties()->with('values')->get()->map->format()->groupBy('group')->toArray();
        $ps = [];
        foreach ($pros as $key => $value) {
            $ps[] = [
                'group' => $key,
                'list' => collect($value)->map(function ($item) {
                    unset($item['group']);
                    return $item;
                })
            ];
        }
        return  $ps;
    }
}
