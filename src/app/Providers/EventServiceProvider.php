<?php

namespace App\Providers;


use App\Events\RegistredEvent;
use App\Events\SyncStoreEvent;
use App\Events\SyncProductEvent;
use App\Events\SyncCategoryEvent;
use App\Events\ResetPasswordEvent;
use App\Listeners\RegistedListener;
use App\Listeners\SyncStoreListener;
use App\Listeners\SyncProductListener;
use App\Listeners\SyncCategoryListener;
use App\Listeners\ResetPasswordListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        SyncProductEvent::class => [
            SyncProductListener::class,
        ],
        SyncCategoryEvent::class => [
            SyncCategoryListener::class,
        ],
        SyncStoreEvent::class => [
            SyncStoreListener::class,
        ],
        ResetPasswordEvent::class => [
            ResetPasswordListener::class,
        ],
        RegistredEvent::class => [
            RegistedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
