<?php

namespace App\Jobs;

use App\Services\Sync\SyncPropertyService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncPropertiesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $file_content;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $file_content)
    {
        $this->file_content = $file_content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response =   json_decode($this->file_content, true);
        if (!empty($response['properties'])) {
            SyncPropertyService::load($response['properties']);
        }
        if (!empty($response['property_value'])) {
            SyncPropertyService::loadValues($response['property_value']);
        }
    }
}
