<?php

namespace App\Jobs;

use App\Services\Sync\SyncStoreService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncStoresJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $file_content;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $file_content)
    {
        $this->file_content = $file_content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response =   json_decode($this->file_content, true);
        if (!empty($response['stores'])) {
            SyncStoreService::load($response['stores']);
        }
        if (!empty($response['balance'])) {
            SyncStoreService::loadBalance($response['balance']);
        }
        if (!empty($response['calculations'])) {
            SyncStoreService::loadAmounts($response['calculations']);
        }
    }
}
