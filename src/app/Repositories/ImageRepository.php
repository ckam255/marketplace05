<?php

namespace App\Repositories;

use App\Models\Image;
use App\Services\ImageManageService;

class ImageRepository
{
	private Image $image;

	public function __construct(Image $image)
	{
		$this->image = $image;
	}

	public function all()
	{
		// return $this->image->all()->map->format();
		return $this->image->all();
	}

	public function store($image, int $position, string $folder = null)
	{
		$filename = ImageManageService::upload($image, $folder);
		$path = !empty($folder) ? "{$folder}/{$filename}" : $filename;
		$current = $this->image->create([
			'tabindex' => $position,
			'is_main' => $position === 0,
			'url' => image_path($path)
		]);
		return $current->id;
	}

	public function upload($images, string $folder = "")
	{
		$tabindex = 0;
		$ids = [];
		foreach ($images as $image) {
			$id = $this->store($image, $tabindex, $folder);
			$ids[] = $id;
			$tabindex++;
		}
		return $ids;
	}

	public function download(string $filename)
	{
		if (!$img = $this->image->where('name', $filename)->first()) {
			throw new \Exception("file not found");
		}
		return ImageManageService::download($img->name);
	}

	/**
	 * Display images with params
	 * @param int $width
	 * @param int $height
	 * @param string $path
	 * @return mixed
	 */
	public function show(int $width, int $height, string $path)
	{
		return \Intervention\Image\Facades\Image::make(storage_path('app/images/' . $path))
			->fit($width, $height)
			->response();
	}

	/**
	 * Display image with queries parameters
	 * @param string $path
	 * @return mixed
	 */
	public function display(string $path)
	{
		$image =  \Intervention\Image\Facades\Image::make(storage_path('app/images/' . $path));
		if (request()->exists('w') && request()->exists('h')) {
			return $image->fit(request()->get('w'), request()->get('h'))->response();
		}
		return $image->response();
	}

	/**
	 * Delete image by ID
	 * @param array $id image ID
	 * @return void 
	 */
	public function delete(int $id)
	{
		$image = Image::find($id);
		if ($image === null) {
			abort(404, "Фотография с ID {$id} не существует");
		}
		$image->products()->detach();
		$image->delete();
		ImageManageService::delete($image->name);
	}


	/**
	 * Remove all images in 
	 * @param array $images image ID list
	 * @return void 
	 */
	public function remove(array $images)
	{
		foreach ($images as $id) {
			$this->delete($id);
		}
	}

	/**
	 * Remove useless images from db and disk
	 */
	public static function removeUseless()
	{
		$images = Image::whereDoesntHave('product')->get();
		$images->each(function ($img) {
			$img->delete();
			info(printf("s% deleted", $img->url));
		});
	}

	/**
	 * Update product images
	 * @param \App\Models\Product $product
	 * @param array $images images id list
	 * @return array
	 */
	public static function updateImages($images, $product)
	{
		foreach ($images as $imgid) {
			if (!$img = Image::where('id', $imgid)->first()) {
				abort(400, "Изображение с идентификатором:$imgid не найдено");
			}
			$img->update([
				'is_main' => array_search($img->id, $images) === 0,
				'tabindex' => array_search($img->id, $images),
				'product_id' => $product->id
			]);
		}
		return $product->images()->get();
	}
}
