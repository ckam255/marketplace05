<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\Store;

class OrderRepository
{
	private Order $instance;

	public function __construct(Order $order)
	{
		$this->instance = $order;
	}

	public function list()
	{
		$query = $this->instance->all();
		return $query;
	}

	public  function find($id)
	{
		return $this->instance->where('id', (int)$id);
	}

	public function search(array $params)
	{
		$query =  $this->instance;
		$valid = 0;
		foreach ($params as $key => $value) {
			if ($key == 'state') {
				$query = $query->whereHas('status', function ($q) use ($value) {
					$q->where('id', states($value))->orWhere('name', $value);
				});
				$valid++;
			}
			if ($key == 'products') {
				$products  = explode(',', trim($value) ?? '');
				$query = $query->whereHas('sales', fn ($q) => $q->whereIn('product_id', $products));
				$valid++;
			}
			if ($key == 'date') {
				$query = $query->where('created_at', $value);
				$valid++;
			}
		}
		if ($valid === 0) {
			abort(400, 'No params found');
		}
		return  $query;
	}
}
