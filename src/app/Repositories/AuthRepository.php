<?php

namespace App\Repositories;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\PasswordReset;
use App\Events\RegistredEvent;
use App\Events\ResetPasswordEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;

class AuthRepository extends BaseRepository
{

    public function login(array $params)
    {
        $user = User::where('email', $params['email'])->first();
        if (!$user || !Hash::check($params['password'], $user->password)) {
            abort(400, 'Указанные данные неверны.');
        }
        if (!$user->is_active) {
            abort(400, 'Вы еще не подтвердили свою учетную запись');
        }
        $token = $user->createToken($params['device_name']);
        $user['access_token'] = $token->plainTextToken;
        $user['expire_at'] = Carbon::parse($token->accessToken->created_at)->addMonth(12);
        return $user;
    }


    /**
     * Create new user
     */
    public function register(array $params)
    {
        $user = User::create(array_merge(
            $params,
            ['password' => Hash::make($params['password'])]
        ));
        $user->store()->create([
            'name' => $user->name,
            'contact' => $params['phone'] ?? null
        ]);
        event(new RegistredEvent($user->email));
        return $user;
    }

    /**
     * Confirm Email address
     */
    public function confirm(string $token)
    {
        $user = $this->verifyToken($token);
        PasswordReset::where('email', $user->email)->delete();
        $user->email_verified_at = now();
        $user->is_active = true;
        $user->save();
        $user->store->update(['status_id' => 11]);
        return $user;
    }

    public function currentUser()
    {
        $user = auth()->user();
        $token = $user->currentAccessToken();
        $expire_at = Carbon::parse($token->created_at)->addMonth(12);
        $days = $expire_at->diffInDays(Carbon::now());
        if ($days <= 0) {
            $token->delete();
            abort(400, "Срок действия вашей учетной записи истек с {$expire_at}");
        }
        $user['expire_at'] = $expire_at;
        $user->store->append('status', 'legal', 'tariff', 'documents');
        return $user;
    }

    public function logout()
    {
        if (request('forked')) {
            $this->clearUserTokens();
        } else {
            request()->user()->currentAccessToken()->delete();
        }
    }

    public function forgotPassword(string $email)
    {
        event(new ResetPasswordEvent($email));
        return ['message' => 'Письмо успешно отправлено. Пожалуйста, проверьте свою электронную почту!'];
    }

    public function resetPassword(array $request)
    {
        $user = $this->verifyToken($request['token']);
        PasswordReset::where('email', $user->email)->delete();
        $this->clearUserTokens($user);
        $user->password = Hash::make($request['password']);
        $user->save();
        return $user;
    }

    public function changePassword(array $request)
    {
        if (Hash::check($request['old_password'], auth()->user()->password)) {
            $user = User::findOrFail(Auth::user()->id);
            $user->password = Hash::make($request['password']);
            $user->save();
            $this->clearUserTokens();
            return '';
        }
        abort(400, 'Текущий пароль неверен.');
    }

    public function refreshToken()
    {
        $user = auth()->user();
        $oldToken = $user->currentAccessToken();
        $token = $user->createToken($oldToken->name);
        $oldToken->delete();
        $expire_at = Carbon::parse($token->accessToken->created_at)->addMonth(12);
        return [
            'access_token' => $token->plainTextToken,
            'expire_at' =>  $expire_at
        ];
    }

    public function currentAccessToken()
    {

        $token = auth()->user()->currentAccessToken();
        $expire_at = Carbon::parse($token->created_at)->addMonth(12);
        return [
            'device' => $token->name,
            'last_used_at' =>  $token->last_used_at,
            'expire_at' =>  $expire_at,
            // 'abilities' => $token->abilities,
            'meta' => $this->unset($token->tokenable, 'is_admin', 'is_superuser')
        ];
    }

    public function accessTokens()
    {
        return auth()->user()->tokens;
    }

    private function clearUserTokens(User $user = null)
    {
        if ($user) {
            $user->tokens()->delete();
        } else {
            auth()->user()->tokens()->delete();
        }
    }

    /**
     * @param $token | token
     * @param $limit_time | limit time in seconds=3600 
     */
    public function verifyToken($token, int $limit_time = 3600)
    {
        if (!$passwordReset = PasswordReset::where('token', $token)->first()) {
            abort(400, 'Неверный токен');
        }
        $created = Carbon::parse($passwordReset->created_at)->addHours($limit_time); // 
        $diff = $created->diffInHours(Carbon::now());
        if ($diff >  $limit_time * 24) {
            $passwordReset->delete();
            abort(400, "Срок действия вашего токена истек");
        }
        if (!$user = User::where('email', $passwordReset->email)->first()) {
            abort(400, `$passwordReset->email не существует!`);
        }
        return $user;
    }

    private function unset($user, ...$fields)
    {
        foreach ($fields as $field) {
            unset($user[$field]);
        }
        return $user;
    }
}
