<?php

namespace App\Repositories;

use App\Events\SyncStoreEvent;
use App\Models\Document;
use App\Models\Store;

class StoreRepository
{
	public OrderRepository $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}

	/**
	 * List orders
	 */
	public function orders()
	{
		$data = $this->orderRepository->list()
			->where('store_id', Store::auth()->id);
		return $data->map->format();
	}

	/**
	 * Search orders 
	 */
	public function searchOrders(array $params)
	{
		return  $this->orderRepository->search($params)
			->where('store_id', Store::auth()->id)
			->get()->map->format();
	}

	/**
	 * Get order by id
	 */
	public function getOrderById($id)
	{
		if (!$order = $this->orderRepository->find($id)
			->where('store_id', Store::auth()->id)
			->first()) {
			abort(400, "У вас нет заказа с id: $id");
		}
		return $order->format();
	}

	/**
	 * Get current store list amounts
	 */
	public function amounts()
	{
		return Store::auth()->amounts()->get();
	}

	/**
	 * Update Current store
	 */
	public function updateCurrent(array $payload)
	{
		$store =  Store::auth();
		if (key_exists('legal', $payload)) {
			$legal =  array_merge(
				$payload['legal'],
				['address' => json_encode($payload['legal']['address'])]
			);
			if (!key_exists('id', $payload['legal'])) {
				$store->legal()->create($legal);
			} else {
				$store->legal()->update();
			}
		}
		$payload = collect($payload)->except('legal')->toArray();
		$payload['address'] = json_encode($payload['address']);
		if ($store->isModerable()) {
			$payload['status_id'] = 12;
			event(new SyncStoreEvent([$store->id]));
		}
		$store->update($payload);
		return $store;
	}

	/**
	 * Get current store documents list
	 */
	public function documents()
	{
		return Store::auth()->documents()->get();
	}

	/**
	 * Upload current store document
	 */
	public function uploadDocument($file, $payload)
	{
		$store =  Store::auth();
		if (!key_exists('id', $payload)) {
			$doc = new Document();
			$doc->store_id = $store->id;
			$doc->name = $payload['name'];
		} else {
			$doc = $store->document()->find($payload['id']);
		}
		$filename = uhex() . '.' . strtolower($file->getClientOriginalExtension());
		$file->storeAs('documents', $filename);
		$doc->url = document_path($filename);
		$doc->save();

		if ($store->isModerable()) {
			$store->update(['status_id' => 12]);
			event(new SyncStoreEvent([$store->id]));
		}
		return $doc;
	}
}
