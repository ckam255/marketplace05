<?php

namespace App\Repositories;

use App\Events\SyncCategoryEvent;
use App\Models\Category;

class CategoryRepository extends BaseRepository
{
	private Category $instance;

	public function __construct(Category $instance)
	{
		$this->instance = $instance;
	}

	public function list()
	{
		return $this->instance->paginate()->get()->map->format(
			$this->queries()->hasChildren,
			$this->queries()->hasProps
		);
	}

	public function getById($id)
	{
		return $this->instance->findOrFail($id)->format(
			$this->queries()->hasChildren,
			$this->queries()->hasProps
		);
	}


	/**
	 * Create new Category.
	 *
	 * @param  array  $params
	 * @return \App\Models\Category
	 */
	public function create(array $params)
	{
		$newCategory = $this->instance->create($params); // create new category
		//event(new SyncCategoryEvent($newCategory)); // send new category to 1C
		return $newCategory;
	}

	public function update(int $id, array $params)
	{
		if (!$category = Category::where('id', $id)->first()) {
			throw new \Exception('Категория  не существует');
		}

		if (!empty($params['name'])) {
			$category->name = $params['name'];
		}
		if (!empty($params['description'])) {
			$category->description = $params['description'];
		}
		if (!empty($params['parent_id'])) {
			$category->parent_id = $params['parent_id'];
		}
		$category->save();
		//	event(new SyncCategoryEvent($category)); // send updated category to 1C

		return $category;
	}

	/**
	 * search categories
	 */
	public function search($q)
	{
		if (empty($q) === 0) {
			abort(400, 'Введите назвние бданда');
		}
		$query =  $this->instance->where('name', 'ILIKE', '%' . $q . '%')
			->orWhere('description', 'ILIKE', '%' . $q . '%');

		return $this->instance->withPagination($query)->get()->map->format(
			$this->queries()->hasChildren,
			$this->queries()->hasProps
		);
	}

	protected function queries()
	{
		$withChildren = false;
		$withProps = false;
		if (request()->exists('with')) {
			$params = explode(',', trim(request()->get('with')));
			$withChildren = in_array('children', $params);
			$withProps = in_array('props', $params);
		}
		$obj = new \stdClass();
		$obj->hasChildren = $withChildren;
		$obj->hasProps = $withProps;
		return $obj;
	}
}
