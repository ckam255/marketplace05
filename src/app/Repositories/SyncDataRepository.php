<?php

namespace App\Repositories;

use App\Jobs\SyncAccountingsJob;
use App\Jobs\SyncBrandsJob;
use App\Jobs\SyncCategoryJob;
use App\Jobs\SyncCategoryPropertyTemplateJob;
use App\Jobs\SyncDataJob;
use App\Jobs\SyncProductPriceJob;
use App\Jobs\SyncProductPropertyValuesJob;
use App\Jobs\SyncProductQuantityJob;
use App\Jobs\SyncProductsJob;
use App\Jobs\SyncPropertiesJob;
use App\Jobs\SyncPropertyValuesJob;
use App\Jobs\SyncOrderJob;
use App\Jobs\SyncOrdersJob;
use App\Jobs\SyncStoreBalanceJob;
use App\Jobs\SyncStoresJob;
use App\Jobs\SyncTariffsJob;

class SyncDataRepository
{

	public function syncAll(string $path)
	{
		if (!file_exists($path)) {
			throw new \Exception($path . '  не существует');
		}
		$file_content = file_get_contents($path);
		//$response =   json_decode($file_content, true);
		dispatch(new SyncDataJob($file_content));
		return [
			'success' => true,
			'exeption' => ''
		];
	}


	public function syncBrands(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncBrandsJob($file_content));
			return 'Synchronization brands in process...';
		}
		throw new \Exception($path . '  does not exists');
	}

	public function syncCategories(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncCategoryJob($file_content));
			return 'Synchronization categories in process...';
		}
		throw new \Exception($path . '  does not exists');
	}

	public function syncStores(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncStoresJob($file_content));
			return 'Synchronization stores in process...';
		}
		throw new \Exception($path . '  does not exists');
	}

	public function syncProducts(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncProductsJob($file_content));
			return 'Synchronization products in process...';
		}
		throw new \Exception($path . '  does not exists');
	}

	public function syncTariffs(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncTariffsJob($file_content));
			return 'Synchronization tariffs in process...';
		}
		throw new \Exception($path . '  does not exists');
	}

	public function syncProperties(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncPropertiesJob($file_content));
			return 'Synchronization properties in process...';
		}
		throw new \Exception($path . '  does not exists');
	}

	public function syncOrders(string $path)
	{
		if (file_exists($path)) {
			$file_content = file_get_contents($path);
			dispatch(new SyncOrdersJob($file_content));
			return 'Synchronization orders in process...';
		}
		throw new \Exception($path . '  does not exists');
	}
}
