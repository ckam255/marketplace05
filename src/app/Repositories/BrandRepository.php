<?php

namespace App\Repositories;

use App\Models\Brand;

class BrandRepository
{

	private Brand $instance;

	public function __construct(Brand $brand)
	{
		$this->instance = $brand;
	}


	public function list()
	{
		$query = $this->instance->where('name', '!=', '')->where('name', '!=', null);
		return $this->instance->withPagination($query, $query->count());
	}


	public function getById(int $id)
	{
		if (!$brand = $this->instance->where('id', $id)->first()) {
			abort(404, "Бренд не найден с идентификатором: $id");
		}
		return $brand;
	}


	public function search($q)
	{
		if (empty($q) === 0) {
			abort(400, 'Введите назвние бденда');
		}
		$query = $this->instance->where('name', 'ILIKE', '%' . $q . '%')
			->orWhere('description', 'ILIKE', '%' . $q . '%');
		return $this->instance->withPagination($query);
	}
}
