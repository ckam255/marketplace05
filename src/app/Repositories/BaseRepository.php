<?php

namespace App\Repositories;



class BaseRepository
{


	public function __construct()
	{
	}

	protected function except_handler($exception)
	{
		return ['error' => $exception->getMessage()];
	}

	protected function exceptJson($exception)
	{
		return response()->json(['error' => $exception->getMessage()], 400);
	}
}
