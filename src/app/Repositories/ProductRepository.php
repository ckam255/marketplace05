<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\Property;
use App\Services\Serialiser;
use App\Events\SyncProductEvent;
use App\Models\Store;
use App\Services\ProductFilterService;

class ProductRepository
{

	private Product $instance;

	public function __construct(Product $product)
	{
		$this->instance = $product;
	}

	/**
	 * get authenticated store products
	 */
	public function getAll()
	{
		return $this->instance->list();
	}

	/**
	 * Get product by code
	 */
	public function getById(string $id)
	{
		$product =  $this->instance->getById($id);
		if (!$product) {
			abort(404, "Товар не найден.");
		}
		return $product;
	}

	/**
	 * get authenticated store products
	 */
	public function search()
	{
		return $this->instance->search()->get()->map->toArray();;
	}

	/**
	 * get authenticated store products
	 */
	public function filter()
	{
		return $this->instance->filter()->get()->map->toArray();;
	}

	/**
	 * get authenticated store products
	 */
	public function archived()
	{
		return $this->instance->archived()->get()->map->toArray();;
	}

	/**
	 * Create product for authenticated store use
	 * @params array $attrs
	 */
	public function create(array $attrs)
	{
		$data = collect($attrs)->except('images', 'properties')
			->put('status_id', 1)->toArray();
		$product =  Store::auth()->products()->create($data);
		if (!empty($attrs['images'])) {
			ImageRepository::updateImages($attrs['images'], $product);
		}
		if ($properties = request()->get('properties')) {
			$this->updateOrCreateProps($properties, $product);
		}

		event(new SyncProductEvent([$product->id]));
		return $product;
	}

	/**
	 * Update product detail
	 */
	public function update(array $data, int $id)
	{
		$product = $this->getVerifiedProduct($id);
		$attrs = collect($data)->except(['properties', 'images']);
		if ($this->isModerationNeeded(collect($data))) {
			$attrs->put('status_id', 1);
		}
		$product->update($attrs->toArray());

		if ($images = request()->get('images')) {
			ImageRepository::updateImages($images, $product);
		}
		if ($properties = request()->get('properties')) {
			$this->updateOrCreateProps($properties, $product);
		}
		event(new SyncProductEvent([$product->id]));
		return $product;
	}

	public function updateAttributes(array $data, int $id)
	{
		$product = $this->getVerifiedProduct($id);
		$product->update(
			collect($data)->forget('state')->put('status_id', 1)->toArray()
		);

		if ($state = request()->get('state')) {
			if (in_array($state, ['hidden', 'visible'])) {
				$product->isvisible = states($state);
			}
			if (in_array($state, ['archived', 'unarchived'])) {
				$product->archived = states($state);
			}
			$product->save();
		}
		event(new SyncProductEvent([$product->id]));
		return $product;
	}

	/***
	 *  archive/unarchive list products
	 */
	public function archive(array $params, int $value = 1)
	{
		$ids = $params['products'];
		$products = $this->instance->getInList(collect($ids)->toArray());
		$products->each(function ($product) use ($value) {
			$product->update([
				'archived' => $value
			]);
		});
		event(new SyncProductEvent([$ids]));
		return $this->instance->getInList(collect($ids)->toArray());
	}

	/**
	 * get product images
	 * @param int $id product id
	 * @return array
	 */
	public function getImages(int $id)
	{
		$product = $this->instance->getSingleOrFail($id);
		return $product->images;
	}

	/**
	 * Get  stats of products by status
	 * @return array
	 */
	public function stats()
	{
		$data = array(
			[
				'name' => 'Все',
				'value' => 'all',
				'productCount' => ProductFilterService::current()->total()
			],
			[
				'name' => 'Продажа',
				'value' => 'available',
				'productCount' =>  ProductFilterService::current()->available()->count()
			],
			[
				'name' => 'Нет в наличии',
				'value' => 'unavailable',
				'productCount' => ProductFilterService::current()->unavailable()->count()
			],
			[
				'name' => 'С ошибками',
				'value' => 'wrong',
				'productCount' => ProductFilterService::current()->wrong()->count()
			],
			[
				'name' => 'Проверка',
				'value' => 'checking',
				'productCount' => ProductFilterService::current()->checking()->count()
			],
			[
				'name' => 'Скрытые',
				'value' => 'hidden',
				'productCount' =>  ProductFilterService::current()->hidden()->count()
			],
			[
				'name' => 'Архив',
				'value' => 'archived',
				'productCount' =>  ProductFilterService::current()->archived()->count()
			],
			// [
			// 	'name' => 'Черновик',
			// 	'value' => 'draft',
			// 	'productCount' => ProductFilterService::current()->draft()->count()
			// ],
			// [
			// 	'name' => 'Перемодерация',
			// 	'value' => 'remoderation',
			// 	'productCount' => ProductFilterService::current()->remoderation()->count()
			// ]
		);
		return $data;
	}

	/**
	 * Check readonly product
	 * @param int $id product id
	 * @return \App\Models\Product
	 */
	protected function getVerifiedProduct(int $id)
	{
		$product = $this->instance->getSingleOrFail($id);
		if ($product->isReadonly()) {
			abort(401, "Вы можете редактировать продукт, импортированный из файла yaml.");
		}
		if (!empty($data['category_id']) && $product->category_id !== $data['category_id']) {
			$product->properties()->detach();
		}

		return $product;
	}

	/**
	 * Create or update product properties
	 * @param \App\Models\Product $product
	 * @return array
	 */
	protected function updateOrCreateProps(array $properties, Product $product)
	{
		foreach ($properties as $property) {
			if (!$prop =  Property::find($property['property_id'])) {
				abort(404, "Своиство (" . $property['property_id'] . ") не найдена ");
			}
			$product->properties()->syncWithoutDetaching([
				$prop->id => [
					'is_value_id' =>  $property['is_value_id'] ?? false,
					'value' =>  json_encode($property['value'])
				]
			]);
		}
		return $product->properties()->get()->map->export();
	}

	/**
	 *  check if product status must modify
	 * @param \Illuminate\Support\Collection $data
	 * @return bool
	 */
	protected function isModerationNeeded(\Illuminate\Support\Collection $data)
	{
		if ($data->except(['price', 'quantity', 'state'])->count() === 0) {
			return false;
		}
		return true;
	}
}
