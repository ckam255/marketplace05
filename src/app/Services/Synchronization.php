<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class Synchronization
{
    public static function post(array $body)
    {
        set_time_limit(-1);

        try {
            $url = env('SYNC_1C_URL', 'http://1c.05.ru:86/1C_UT11_DEV_NEW/hs/api/site/marketplace/sync');
            $username = env('SYNC_1C_USER', 'site05ru_user');
            $password = env('SYNC_1C_PWD', '');

            info(printf('Request Body: [%s]' . PHP_EOL, json_encode($body)));

            $response = Http::withBasicAuth($username, $password)
                ->contentType('json')
                ->acceptJson()
                ->timeout(10)
                ->post($url, $body);

            info(printf('Request Response: [%s]' . PHP_EOL, $response->json()));

            if ($response->successful()) {
                return ['error' => true, 'result' => $response->json()];
            }

            if ($response->status() === 401) {
                return ['error' => true, 'result' => '401|UNAUTHORIZED' . PHP_EOL];
            } else {
                return ['error' => true, 'result' => $response->status() . ' | Some errors occured' . PHP_EOL];
            }
        } catch (\Exception $ex) {
            return ['error' => true, 'result' => $ex->getMessage() . PHP_EOL];
        }
    }
}
