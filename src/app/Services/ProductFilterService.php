<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;

class ProductFilterService
{
    private static  $product;
    private static $instance;

    public static function current($query = null)
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        self::$product  = $query ?? Store::auth()->products(); // Product::where('store_id', Store::auth()->id);
        return self::$instance;
    }

    public  function total()
    {
        return self::$product->get()->count();
    }

    public  function available() // Продажа
    {
        return self::$product->where('isvisible', '=', 1)
            ->where('archived', '=', 0)
            ->where('quantity', '>', 0)
            ->whereHas('status', fn ($q) => $q->where('status.id', 4));
    }

    public  function unavailable() // Нет в наличии
    {
        return self::$product->where('isvisible', '=', 1)
            ->where('archived', '=', 0)
            ->where('quantity', '=', 0)
            ->whereHas('status', fn ($q) => $q->where('status.id', 4));
    }

    public  function checking() // Проверка
    {
        return self::$product->where('isvisible', '=', 1)
            ->where('archived', '=', 0)
            ->whereHas('status', fn ($q) => $q->where('status.id', 1));
    }


    public  function remoderation() // Премоoдерация
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 3));
    }

    public  function wrong() // С ошибками
    {
        return self::$product->where('isvisible', '=', 1)
            ->whereHas('status', fn ($q) => $q->where('status.id', 2));
        // ->whereHas('status', fn ($q) => $q->where('status.id', 2)->whereNotIn('status.id', [1, 3, 4, 5, 6]));
    }

    public  function hidden()
    {
        return self::$product->where('isvisible', '=', 0);
        // ->whereHas('status', fn ($q) => $q->whereNotIn('status.id', [1, 3, 4, 5, 6]));
    }

    public  function archived() // Архив
    {
        return self::$product->where('archived', '=', 1);
    }


    public  function draft() // Черновик
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 5));
    }

    public  function byCategories(array $categories)
    {
        return self::$product->whereHas('category', fn ($q) => $q->whereIn('id', $categories));
    }
    /*
    *Все* - все товары данного продавца во всех статусах.
    *Продажа* - товары со статусом “Активен”, остаток больше 0, “Скрытый - нет”, Архивный - нет
    *Нет в наличии* - товары со статусом “Активен”, остаток 0, “Скрытый - нет”, Архивный - нет
    *С ошибками* - товары со статусом “С ошибками”, “Скрытый - нет”, Архивный - нет. Это товары которые пользователь должен исправить и отправить на модерацию
    *Проверка* - товары со статусом “На проверке”, “Скрытый - нет”, Архивный - нет
    *Скрытые* - товары,  у которых “Скрытый - да” независимо от остальных реквизитов, Архивный - нет
    *Архив* - товары с пометкой удаления независимо от остальных реквизитов. , Архивный - да
*/
}
