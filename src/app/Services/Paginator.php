<?php


namespace App\Services;

trait Paginator
{
    public function paginate($query, $per_page = 0)
    {
        $per_page = $per_page === 0 ? $query->count() : $per_page;
        $p = abs((int)request('page'));
        $page =  $p > 0 ? $p : 1;
        $ppage = abs((int)request('per_page'));
        $per_page = $ppage > 0 ? $ppage : $per_page;
        $offset = ($per_page * $page - $per_page);
        return toArray($query->skip($offset)->take($per_page));
    }
}
