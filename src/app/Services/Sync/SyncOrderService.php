<?php

namespace App\Services\Sync;

use Carbon\Carbon;
use App\Models\Sale;
use App\Models\Order;
use App\Models\Store;
use App\Models\Product;
use Illuminate\Support\Facades\Log;

class SyncOrderService
{
    public static function load($sales)
    {
        Log::info(printf('--- Synchronization sales started ---' .  PHP_EOL));
        sleep(1);

        $total = count($sales);
        $count = 0;

        foreach ($sales as  $c) {
            $date = Carbon::parse($c['date']);
            if (!$order = Order::where('number', $c['num'])->where('created_at', $date)->first()) {
                $order = new Order();

                $order->number = $c['num'];
                $order->type = $c['type'];
                $order->sum = $c['sum'];
                $order->status_id = (bool)$c['type'] == true ? 21 : 20;
                $order->created_at = $date;

                if ($store = Store::where('code_1c', $c['store_id'])->first()) {
                    $order->store_id = $store->id;
                }
                $order->save();

                foreach ($c['products'] as $p) {
                    if ($product = Product::where('code_1c', $p['id'])->orWhere('parent_id', $p['id'])->first()) {
                        Sale::create([
                            'order_id' => $order->id,
                            'product_id' => $product->id,
                            'quantity' => $p['quantity'],
                            'created_at' => $date,
                            'sum' => $p['sum'],
                        ]);
                    } else {
                        info(printf("(product_id)=(" . $p['id'] . ") is not present in table 'products'" .  PHP_EOL));
                    }
                }
                $count++;
                info(printf($count . '/' . $total . ' treated' . ' (sales)'  .  PHP_EOL));
                usleep(100);
            }
        }
    }
}
