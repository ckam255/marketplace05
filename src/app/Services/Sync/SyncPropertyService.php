<?php

namespace App\Services\Sync;

use App\Models\Property;
use App\Models\PropertyValue;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncPropertyService
{
    public static function load($properties)
    {
        Log::info(printf('--- Synchronization properties started ---' .  PHP_EOL));
        sleep(1);

        $total = count($properties);
        $count = 0;

        foreach ($properties as $x) {
            if (!$property = Property::where('code_1c', $x['id'])->first()) {
                $property = new Property();
                $property->code_1c = $x['id'];
            }
            $property->name = $x['name'];
            $property->type = $x['type'];
            $property->description = $x['description'];
            $property->save();
            $count++;
            info(printf('[%s] %s/%s treated  (properties)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }

    public static function loadValues($values)
    {
        Log::info(printf('--- Synchronization property_values started ---' .  PHP_EOL));
        sleep(1);

        $total = count($values);
        $count = 0;

        foreach ($values as $x) {
            if ($property = Property::where('code_1c', $x['property_id'])->first()) {
                if (!$pvalue = PropertyValue::where('code_1c', $x['id'])->first()) {
                    $pvalue = new PropertyValue();
                    $pvalue->code_1c = $x['id'];
                }
                $pvalue->property_id = $property->id;
                $pvalue->value = $x['value'];
                $pvalue->save();
                $count++;
                info(printf('[%s] %s/%s treated  (property-value)' .  PHP_EOL, now(),  $count, $total));
                usleep(100);
            }
        }
    }
}
