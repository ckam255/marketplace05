<?php

namespace App\Services\Sync;

use Carbon\Carbon;
use App\Models\Sale;
use App\Models\User;
use App\Models\Order;
use App\Models\Store;
use App\Models\Amount;
use App\Models\Legal;
use App\Models\Status;
use App\Models\Tariff;
use App\Models\Worktime;
use App\Services\Synchronization;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class SyncStoreService
{
    public static function syncTo(array $ids)
    {
        $stores = [];
        foreach ($ids as $id) {
            if ($store = Store::where('id', $id)->first()) {
                $stores[] = $store->exportable();
            }
        }
        $body =  [
            'stores' => $stores,
        ];
        $response = Synchronization::post($body);
        if ($response['error']) {
            info(printf(
                '[%s] Error: ' . PHP_EOL . 'message:%s' . PHP_EOL,
                now(),
                $response['result']
            ));
            return false;
        }
        $data = $response['result'];
        $errors = $data['stores']['errors'];
        $stores = $data['stores']['new'];
        if (count($errors) > 0) {
            foreach ($errors as $err) {
                info(printf('[%s] Error: store: (%s/%s): ' . PHP_EOL . 'message:%s' . PHP_EOL, now(), $err['id'],   $err['code_1c'], $err['description']));
            }
            return false;
        }
        foreach ($stores as $x) {
            $s = Store::findOrFail($x['id']);
            $s->update(['code_1c' => $x['code_1c']]);
            info(printf('[%s] Success: store(|%s/%s) successfully sync' . PHP_EOL, now(), $s->id,   $s->code_1c));
        }
        return true;
    }

    public static function load($stores)
    {
        Log::info(printf('--- Synchronization stores started ---' .  PHP_EOL));
        sleep(1);

        $total = count($stores);
        $count = 0;
        foreach ($stores as $x) {

            if (!$store = Store::where('code_1c', $x['id'])->first()) {
                $store = new Store();
                $store->code_1c = $x['id'];
            }
            $store->name = $x['name'];
            $store->contact = $x['contact_phone'];
            $store->website = $x['site'];
            $store->address = json_encode([
                'city' =>  $x['postal_address'],
                'street' => $x['warehouse_address'],
            ]);

            if (!empty($x['login']) && $store->user_id === null) {
                if (!$user = User::where('email', $x['login'])->first()) {
                    $user = User::create(['name' => $x['name'], 'email' => $x['login'], 'password' => Hash::make($x['password'])]);
                }
                $store->user_id = $user->id;
                if ($user->tokens) {
                    $user->tokens()->delete();
                }
            }

            if (!empty($x['tariff'])) {
                if ($tariff = Tariff::where('code_1c', $x['tariff'])->first()) {
                    $store->tariff_id = $tariff->id;
                }
            }

            if (!empty($x['status'])) {
                if ($status = Status::where('id', (int)$x['status'])->first()) {
                    $store->status_id = $status->id;
                }
            }

            $store->save();

            /*
            if (!$legal = $store->legal()->first()) {
                $legal = new Legal();
            }
            $legal->store_id = $store->id;
            $legal->name = $x['legal_name'];
            $legal->director = $x['director'];
            $legal->legal_type = $x['form_of_ownership'];
            $legal->address = json_encode($x['legal_address']);
            $legal->inn = $x['inn'];
            $legal->bik = $x['bik'];
            $legal->kpp = $x['kpp'];
            $legal->ogrn = $x['ogrn'];
            $legal->ogrnip = $x['ogrnip'] ?? null;
            $legal->payment_account = $x['payment_account'];
            $legal->save();
             */

            if (!$worktime = $store->worktime()->first()) {
                $worktime = new Worktime();
            }
            $worktime->store_id = $store->id;
            $worktime->start = $x['work_schedule_start'];
            $worktime->end = $x['work_schedule_end'];
            $worktime->save();


            $count++;
            info(printf('[%s] %s/%s treated  (store)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }

    public static function loadAmounts($accountings)
    {
        Log::info(printf('--- Synchronization amounts started ---' .  PHP_EOL));
        sleep(1);
        $total = count($accountings);
        $count = 0;

        foreach ($accountings as $x) {
            if ($store = Store::where('code_1c', $x['store_id'])->first()) {
                if (!$amount = Amount::where('number', $x['num'])
                    ->where('created_at', Carbon::parse($x['date']))
                    ->where('store_id', $store->id)->first()) {
                    Amount::create([
                        'number' => $x['num'],
                        'store_id' => $store->id,
                        'created_at' => Carbon::parse($x['date']),
                        'amount' => $x['sum'],
                    ]);

                    $count++;
                    info(printf('[%s] %s/%s treated  (amounts)' .  PHP_EOL, now(),  $count, $total));
                    usleep(100);
                }
            }
        }
    }

    public static function loadBalance($balances)
    {
        Log::info(printf('--- Synchronization balance started ---' .  PHP_EOL));
        sleep(1);

        $total = count($balances);
        $count = 0;

        foreach ($balances as $c) {
            if ($store = Store::where('code_1c', $c['store_id'])->first()) {
                $store->balance = $c['amount'];
                $store->save();
                $count++;
                info(printf('[%s] %s/%s treated  (balance)' .  PHP_EOL, now(),  $count, $total));
                usleep(100);
            }
        }
    }
}
