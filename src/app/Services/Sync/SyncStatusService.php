<?php

namespace App\Services\Sync;

use App\Models\Status;
use Illuminate\Support\Facades\Log;

class SyncStatusService
{
    public static function load($data)
    {
        Log::info(printf('--- Synchronization statuses started ---' .  PHP_EOL));
        sleep(1);
        $total = count($data);
        $count = 0;

        foreach ($data as $d) {
            if ($d['id'] === 0) {
                break;
            }
            if ($status = Status::where('id', $d['id'])->first()) {
                $status->title = $d['name'];
                $status->save();
                $count++;
                info(printf('[%s] %s/%s treated  (status)' .  PHP_EOL, now(),  $count, $total));
                usleep(100);
            }
        }
    }
}
