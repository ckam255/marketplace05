<?php

namespace App\Services\Sync;

use App\Models\Brand;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncBrandService
{
    public static function load($brands)
    {
        Log::info(printf('--- Synchronization brands started ---' .  PHP_EOL));
        sleep(1);
        $total = count($brands);
        $count = 0;

        foreach ($brands as $current) {
            if (!$brand = Brand::where('code_1c', $current['id'])->first()) {
                $brand = new Brand();
                $brand->code_1c = $current['id'];
            }
            $brand->name = $current['name'];
            $brand->save();
            $count++;
            info(printf('[%s] %s/%s treated  (brands)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }
}
