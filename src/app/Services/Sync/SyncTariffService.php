<?php

namespace App\Services\Sync;


use App\Models\Tariff;
use App\Models\Category;
use Illuminate\Support\Facades\Log;

class SyncTariffService
{
    public static function load($tariffs)
    {
        Log::info(printf('--- Synchronization tariffs started ---' .  PHP_EOL));
        sleep(1);

        $total = count($tariffs);
        $count = 0;

        foreach ($tariffs as $current) {
            if (!$tariff = Tariff::where('code_1c', $current['id'])->first()) {
                $tariff = new Tariff();
                $tariff->code_1c = $current['id'];
            }
            $tariff->name = $current['name'];
            $tariff->commission = $current['commission'];
            $tariff->commission_acquiring =  $current['commission_acquiring'];
            $tariff->min_commission_RUB =  $current['min_commission_RUB'];
            $tariff->save();

            if (!empty($current['by_category'])) {
                self::setCategoryTariff($current['by_category'], $tariff->id);
            }
            $count++;
            info(printf('[%s] %s/%s treated  (tariff)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }

    private static function setCategoryTariff(array $categories, $tariff)
    {
        foreach ($categories as $cat) {
            if ($category = Category::where('code_1c', $cat)->first()) {
                $category->update(['tariff_id' => $tariff]);
            }
        }
    }
}
