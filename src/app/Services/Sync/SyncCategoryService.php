<?php

namespace App\Services\Sync;

use App\Models\Category;
use App\Models\Property;
use Illuminate\Support\Facades\Log;

class SyncCategoryService
{
    public static function load($categories)
    {
        Log::info('--- Synchronization categories started ---' .  PHP_EOL);
        sleep(1);
        $total = count($categories);
        $count = 0;

        foreach ($categories as $x) {
            if (!$category = Category::where('code_1c', $x['id'])->first()) {
                $category = new Category();
                $category->code_1c = $x['id'];
            }
            $category->name = $x['name'];
            if (!empty($x['parent_id']) && $parent = Category::where('code_1c', $x['parent_id'])->first()) {
                $category->parent_id = $parent->id;
            }
            $category->save();
            $count++;
            info(printf('[%s] %s/%s treated  (category)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }

    public static function loadPropertyTemplate($data)
    {
        Log::info(printf('--- Synchronization category_properties_template started ---' .  PHP_EOL));
        sleep(1);
        $total = count($data);
        $count = 0;
        foreach ($data as $x) {
            if (($prop  = Property::where('code_1c', $x['id'])->first())
                && ($category = Category::where('code_1c', $x['category_id'])->first())
            ) {
                $category->properties()->syncWithoutDetaching([$prop->id => ['group_name' => $x['group_name']]]);
            }
            $count++;
            info(printf('[%s] %s/%s treated  (category_properties)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }
}
