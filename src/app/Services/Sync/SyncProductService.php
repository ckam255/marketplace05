<?php

namespace App\Services\Sync;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use App\Models\Status;
use App\Models\Product;
use App\Models\Property;
use App\Models\Store;
use App\Services\Synchronization;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;


class SyncProductService
{

    public static function syncTo(array $ids)
    {
        $products = [];
        foreach ($ids as $id) {
            if ($product = Product::where('id', $id)->first()) {
                $products[] = $product->exportable();
            }
        }
        $body =  [
            'products' => $products,
        ];
        $response = Synchronization::post($body);
        if ($response['error']) {
            info(printf(
                '[%s] Error: ' . PHP_EOL . 'message:%s' . PHP_EOL,
                now(),
                $response['result']
            ));
            return false;
        }
        $data = $response['result'];
        $errors = $data['products']['errors'];
        $products = $data['products']['new'];
        if (count($errors) > 0) {
            foreach ($errors as $err) {
                info(printf('[%s] Error: product: (%s/%s): ' . PHP_EOL . 'message:%s' . PHP_EOL, now(), $err['id'],   $err['code_1c'], $err['description']));
            }
            return false;
        }
        foreach ($products as $x) {
            $p = Product::findOrFail($x['id']);
            $p->update(['code_1c' => $x['code_1c']]);
            info(printf('[%s] Success: product(|%s/%s) successfully sync' . PHP_EOL, now(), $p->id,   $p->code_1c));
        }
        return true;
    }



    public static function load($products)
    {
        Log::info(printf('--- Synchronization products started ---' .  PHP_EOL));
        sleep(1);
        $total = count($products);
        $count = 0;
        foreach ($products as $prod) {

            if (!$product = Product::where('code_1c', $prod['id'])->first()) {
                $product = new Product();
                $product->code_1c = $prod['id'];
            }
            $product->parent_id = !empty($prod['product_id']) ? $prod['product_id'] : null;
            $product->name = $prod['name'];
            $product->barcode = $prod['barcode'];
            $product->description = $prod['description'];
            $product->slug = $prod['article'];
            $product->weight = floatval($prod['weight']);
            $product->length = floatval($prod['length']);
            $product->height = floatval($prod['height']);
            $product->width = floatval($prod['width']);
            $product->isvisible = !$prod['hidden'];

            if (key_exists('store_id', $prod)) {
                if ($store = Store::where('code_1c', $prod['store_id'])->first()) {
                    $product->store_id =  $store->id;
                }
            }

            if (key_exists('category', $prod)) {
                if ($category = Category::where('code_1c', $prod['category'])->first()) {
                    $product->category_id =  $category->id;
                }
            }

            if (key_exists('brand', $prod)) {
                if ($brand = Brand::where('code_1c', $prod['brand'])->first()) {
                    $product->brand_id =  $brand->id;
                }
            }

            if (key_exists('status', $prod)) {
                if ($status = Status::where('id', $prod['status'])->first()) {
                    $product->status_id =  $status->id;
                }
            }

            if (key_exists('price', $prod)) {
                $product->price =  (int)$prod['price'];
            }

            if (key_exists('quantity', $prod)) {
                $product->quantity  =  (int)$prod['quantity'];
            }

            if (key_exists('errors', $prod)) {
                $product->exceptions  =  $prod['errors'];
            }

            if (!empty($prod['yml'])) {
                $product->imported = $prod['yml'] === true ? 'yml' : null;
            }

            $product->save();

            if (!empty($prod['photo'])) {
                self::loadImages($product->id, $prod['photo']);
            }

            $count++;
            info(printf('[%s] %s/%s treated  (product)' .  PHP_EOL, now(),  $count, $total));
            usleep(100);
        }
    }

    public static function loadPrice($prices)
    {
        Log::info(printf('--- Synchronization prices started ---' .  PHP_EOL));
        sleep(1);

        $total = count($prices);
        $count = 0;

        foreach ($prices as $c) {
            $updated = false;
            if ($product = Product::where('code_1c', $c['product_id'])->where('store_id', $c['store_id'])->first()) {
                $product->price = $c['price'];
                $product->save();
                $updated = true;
            }
            $count++;
            info(printf('[%s] %s/%s updated:%s (price=%s) ' .  PHP_EOL, now(),  $count, $total, $updated, $c['product_id']));
            usleep(100);
        }
    }

    public static function loadQuantity($amount)
    {
        Log::info(printf('--- Synchronization products quantity started ---' .  PHP_EOL));
        sleep(1);

        $total = count($amount);
        $count = 0;

        foreach ($amount as $c) {
            $updated = false;
            if ($product = Product::where('code_1c', $c['product_id'])->where('store_id', $c['store_id'])->first()) {
                $product->quantity = $c['quantity'];
                $product->save();
                $updated = true;
            }
            $count++;
            info(printf('[%s] %s/%s updated:%s (price=%s) ' .  PHP_EOL, now(),  $count, $total, $updated, $c['product_id']));
            usleep(100);
        }
    }

    public static function loadProperties($product_property_values)
    {
        Log::info(printf('--- Synchronization product_property started ---' .  PHP_EOL));
        sleep(1);

        $total = count($product_property_values);
        $count = 0;


        foreach ($product_property_values as $current) {
            $created = false;
            if (($product = Product::where('code_1c', $current['product_id'])->first())
                && ($property = Property::where('code_1c', $current['property_id'])->first())
            ) {
                $product->properties()->syncWithoutDetaching([
                    $property->id => [
                        'is_value_id' => $current['is_value_id'],
                        'value' => json_encode($current['value']),
                    ]
                ]);
                $created = true;
            }
            $count++;
            info(printf('[%s] %s/%s treated created:%s (product_property) ' .  PHP_EOL, now(),  $count, $total, $created));
            usleep(100);
        }
    }

    public static function loadImages($product_id, $images)
    {
        usleep(50);
        foreach ($images as $x) {
            if (!empty($x['url'])) {
                if (!$img = Image::where('url',  $x['url'])->first()) {
                    $img = new Image();
                }
                $img->is_main = $x['main'];
                $img->tabindex = (int)$x['num'] - 1;
                $img->url = $x['url'];
                $img->product_id = $product_id;
                $img->save();
                usleep(50);
            }
        }
    }
}
