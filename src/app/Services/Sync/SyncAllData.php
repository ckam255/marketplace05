<?php

namespace App\Services\Sync;

use App\Services\Sync\SyncBrandService;
use App\Services\Sync\SyncCategoryService;
use App\Services\Sync\SyncOrderService;
use App\Services\Sync\SyncProductService;
use App\Services\Sync\SyncPropertyService;
use App\Services\Sync\SyncStatusService;
use App\Services\Sync\SyncStoreService;
use App\Services\Sync\SyncTariffService;

class SyncAllData
{

    public static function load($response, string $path = null)
    {

        if (exists('statuses', $response)) {
            SyncStatusService::load($response['statuses']);
        }

        if (exists('brands', $response)) {
            SyncBrandService::load($response['brands']);
        }

        // $response =   json_decode($file_content, true);
        if (exists('categories', $response)) {
            SyncCategoryService::load($response['categories']);
        }

        if (exists('tariff', $response)) {
            SyncTariffService::load($response['tariff']);
        }

        if (exists('stores', $response)) {
            SyncStoreService::load($response['stores']);
        }

        if (exists('properties', $response)) {
            SyncPropertyService::load($response['properties']);
        }

        if (exists('property_value', $response)) {
            SyncPropertyService::loadValues($response['property_value']);
        }

        if (exists('category_properties_template', $response)) {
            SyncCategoryService::loadPropertyTemplate($response['category_properties_template']);
        }

        if (exists('products', $response)) {
            SyncProductService::load($response['products']);
        }

        if (exists('product_property_value', $response)) {
            SyncProductService::loadProperties($response['product_property_value']);
        }

        if (exists('prices', $response)) {
            SyncProductService::loadPrice($response['prices']);
        }

        if (exists('calculations', $response)) {
            SyncStoreService::loadAmounts($response['calculations']);
        }

        if (exists('amount', $response)) {
            SyncProductService::loadQuantity($response['amount']);
        }

        if (exists('sales', $response)) {
            SyncOrderService::load($response['sales']);
        }

        if (exists('balance', $response)) {
            SyncStoreService::loadBalance($response['balance']);
        }

        if (file_exists($path)) {
            unlink($path);
        }
    }
}
