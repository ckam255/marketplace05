<?php 
namespace App\Services;

use App\Models\Image;

class ImageManageService
{

    public static function upload($image, string $dir = null)
    {
        $path = 'images' . ($dir ? "/{$dir}" : '');
        $filename = ucode() . '.' . strtolower($image->getClientOriginalExtension());
        $image->storeAs($path, $filename);
        return  $filename;
    }

    public static function delete($filename)
    {
        $path = storage_path('app/images/' . $filename);
        if (file_exists($path)) {
            unlink($path);
        }
    }

    public static function resize($file)
    {
        $image = $file->move(storage_path("images/{$file->getBasename()}", $file->getClientOriginalExtension()));
        $formats = [150, 500, 1000, 1200, 1400];
        // dispatch(new ResizeImageJob($image, $formats));
        return $image->getBasename();
    }

    public static function download(string $filename)
    {
        return response()->download(storage_path('app/images/' . $filename));
    }
}
