<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;

class ProductSearchService
{
    private static  $product;
    private static $instance;

    public static function current($query = null)
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        self::$product  = $query ?? Store::auth()->products(); // Product::where('store_id', Store::auth()->id);
        return self::$instance;
    }


    public  function available() // Продажа
    {
        return self::$product->where('quantity', '>', 0);
    }

    public  function unavailable() // Нет в наличии
    {
        return self::$product->where('quantity', '=', 0);
    }


    public  function checking() // Проверка
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 1));
    }


    public  function remoderation() // Премоoдерация
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 3));
    }

    public  function wrong() // С ошибками
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 2));
    }

    public  function hidden()
    {
        return self::$product->where('isvisible', '=', 0);
        // ->whereHas('status', fn ($q) => $q->whereNotIn('status.id', [1, 3, 4, 5, 6]));
    }

    public  function archived() // Архив
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 6));
    }


    public  function draft() // Черновик
    {
        return self::$product->whereHas('status', fn ($q) => $q->where('status.id', 5));
    }

    public  function byCategories(array $categories)
    {
        return self::$product->whereHas('category', fn ($q) => $q->whereIn('id', $categories));
    }
}
