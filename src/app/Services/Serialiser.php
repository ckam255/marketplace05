<?php

namespace App\Services;

class Serialiser
{

    private static $instance;
    private static array $current;

    public static function get($object)
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        self::$current = (array)$object;
        return self::$instance;
    }


    public  function removeKey(string $key)
    {
        unset(self::$current[$key]);
        return $this;
    }

    public  function removeKeys(...$keys)
    {
        foreach ($keys as $key) {
            unset(self::$current[$key]);
        }
        return $this;
    }

    public function add(string $key, $value)
    {
        self::$current[$key] = $value;
        return $this;
    }

    public function toArray()
    {
        return self::$current;
    }

    public  function remove()
    {
        unset(self::$current);
        return $this;
    }
}
