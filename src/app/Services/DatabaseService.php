<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class DatabaseService
{

    public static function dump($type = 'sql')
    {
        if ($type === 'json') {
            return self::dumpJSON();
        }
        return self::dumpSQL();
    }

    public static function restore(string $path, $type = 'sql')
    {
        if ($type === 'json') {
            return self::restoreJSON($path);
        }
        return self::restoreSQL($path);
    }


    public static function dumpSQL()
    {
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $dir = storage_path('backup');
        $path = $dir . '/dump_' . date('dmY_His') . '.sql';
        $cmd = "cd .. && docker-compose exec $host pg_dumpall -c -U $username > $path";
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        exec($cmd);
        return "Database successfully dumped: $path";
    }

    public static function restoreSQL($path)
    {
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $database = env('DB_DATABASE');
        $cmd = "cat $path | docker-compose exec $host psql -U $username";
        $cmd2 = "docker-compose exec $host pg_restore -U $username -d $database $path";
        if (!file_exists($path)) {
            throw new \Exception("$path does not exist");
        }
        exec($cmd);
        return "Database successfully restored";
    }

    public static function dumpJSON()
    {
        $sql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_name <> 'migrations' ;";
        $tables = DB::select($sql);
        $output = collect([]);
        foreach ($tables as $table) {
            $data = DB::select("select * from $table->table_name");
            $output->put($table->table_name, $data);
        }
        $path = export($output->toJson(), 'backup/dump_' . date('dmY_His') . '.json');
        return $path;
    }

    public static function restoreJSON($path)
    {
        $tables = json_decode(file_get_contents(storage_path("backup/$path")), true);
        foreach ($tables as $table => $rows) {
            if (count($rows) > 0) {
                $sql = "INSERT INTO $table (" . implode(", ", array_keys($rows[0])) . ") VALUES ";
                $i = 1;
                foreach ($rows as $cols) {
                    $values = array_values($cols);
                    $sql .= "('" . implode("','", $values) . "')";
                    $sql .= $i < count($rows) ? ', ' : '';
                    $i++;
                }
                $sql = str_replace("''", 'null', $sql);
                DB::insert($sql);
            }
        }
    }
}
