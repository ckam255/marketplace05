<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SyncRequest;
use App\Http\Controllers\Controller;
use App\Repositories\SyncDataRepository;

class SyncDataController extends Controller
{
    private SyncDataRepository $repository;


    public function __construct(SyncDataRepository $repository)
    {
        $header = trim(request()->header('Authorization'));
        if (str_contains($header, 'Bearer')) {
            $this->middleware('auth:sanctum')->except([]);
        } else {
            $this->middleware('auth.basic')->except([]);
        }
        $this->repository = $repository;
    }

    public function all(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncAll($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function categories(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncCategories($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function brands(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncBrands($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function stores(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncStores($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function products(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncProducts($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function tariffs(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncTariffs($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function orders(SyncRequest $request)
    {
        try {
            $path = $this->load($request->validated());
            $response =  $this->repository->syncOrders($path);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json([
                'success' => false,
                'exeption' =>  $ex->getMessage()
            ]);
        }
    }

    public function load($json)
    {
        return  export(json_encode($json), "sync/" . date('dmYHis') . ".json");
    }

    public function upload($file, $filename)
    {
        if (strtolower($file->getClientOriginalExtension())  !== 'json') {
            abort(400, 'Поле marketplace должно быть файлом одного из следующих типов: json');
        }
        $path = [storage_path() . "/sync", "${filename}_" . date('dmYHis') . ".json"];
        $filename =  implode('/', $path);
        if (file_exists($filename)) {
            unlink($filename);
        }
        $file->move($path[0], $path[1]);
        return $filename;
    }
}
