<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth')->except('welcome', 'docs', 'swagger', 'routes');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function docs()
    {
        return view('docs.redoc');
    }

    public function swagger()
    {
        return view('docs.swagger');
    }

    public function routes()
    {
        return view('docs.routes');
    }
}
