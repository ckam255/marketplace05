<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\ImageManageService;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use App\Http\Requests\Product\AddOrRemoveImagesRequest;
use Illuminate\Support\Facades\Route;

class ImageController extends Controller
{

    private ImageRepository $repository;


    public function __construct(ImageRepository $repository)
    {
        $this->middleware('auth:sanctum')->except('display', 'show');
        $this->middleware('user.active')->except('display', 'show');
        $this->repository = $repository;
    }

    public function show(int $width, int $height, string $path)
    {
        try {
            return $this->repository->show($width, $height, $path);
        } catch (\Exception $th) {
            return response(['error' => "Изображение не найдено"], 400);
        }
    }

    public function display(string $path)
    {
        try {
            return $this->repository->display($path);
        } catch (\Exception $ex) {
            return response($ex->getMessage());
            return response(['error' => "Изображение [{$path}] не найдено: " . $ex->getMessage()], 400);
        }
    }

    public function upload(AddOrRemoveImagesRequest $request)
    {
        $images = $request->file('images');
        try {
            $response =  $this->repository->upload($images);
            return response()->json(['images' => $response], 201);
        } catch (\Exception $ex) {
            return response(['error' =>  $ex->getMessage()], 400);
        }
    }

    public function delete(int $id)
    {
        try {
            $response =  $this->repository->delete($id);
            return response()->json($response, 204);
        } catch (\Exception $ex) {
            return response(['error' =>  $ex->getMessage()], 400);
        }
    }

    public function remove(AddOrRemoveImagesRequest $request)
    {
        try {
            $response =  $this->repository->remove($request->validated());
            return response()->json($response);
        } catch (\Exception $ex) {
            return response(['error' =>  $ex->getMessage()], 400);
        }
    }
}
