<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\StoreRepository;
use App\Services\Paginator;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\CreateDocumentRequest;

class StoreController extends Controller
{
    use Paginator;

    public StoreRepository $repository;

    public function __construct(StoreRepository $repository)
    {
        $this->middleware('auth:sanctum')->except([]);
        $this->middleware('user.active');
        $this->repository = $repository;
    }

    public function amounts()
    {
        try {
            $response = $this->paginate($this->repository->amounts());
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function orders()
    {
        try {
            $response = $this->paginate($this->repository->orders());
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function getOrderById($id)
    {
        try {
            $response = $this->repository->getOrderById($id);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function searchOrders()
    {
        try {
            $response = $this->repository->searchOrders(request()->all());
            return response()->json($this->paginate($response));
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function create(CreateStoreRequest $request)
    {
        try {
            $response = $this->repository->updateCurrent($request->validated());
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function documents()
    {
        try {
            $response = $this->repository->documents();
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function uploadDocument(CreateDocumentRequest $request)
    {
        try {
            $response = $this->repository->uploadDocument(
                $request->file('doc'),
                collect($request->validated())->except('doc')->toArray()
            );
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }
}
