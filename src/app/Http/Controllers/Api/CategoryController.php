<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private CategoryRepository $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->middleware('auth:sanctum');
        $this->middleware('user.active');
        $this->repository = $repository;
    }

    public function index()
    {
        return response()->json($this->repository->list());
        //$categories = Cache::rememberForever('categories', fn()=> $this->repository->list()) ;
    }

    public function search(Request $request)
    {
        try {
            $q = $request->input('q');
            $response = $this->repository->search($q);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 404);
        }
    }

    public function store(CreateCategoryRequest $request)
    {
        try {
            $category =  $this->repository->create($request->validated());
            return response()->json($category, 201);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function update(UpdateCategoryRequest $request, string $id)
    {
        // return  $request->validated();
        try {
            $category =  $this->repository->update(
                $id,
                $request->validated()
            );
            return response()->json($category);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    public function show(string $id)
    {
        try {
            return response()->json($this->repository->getById($id));
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Категория  не существует']);
        }
    }
}
