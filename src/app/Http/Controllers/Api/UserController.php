<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except([]);
        $this->middleware('user.active');
    }

    /**
     * User List.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(User::all());
    }

    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return response($user, 200);
    }


    /**
     * Remove User By ID.
     * 
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response("", 204);
    }




    /**
     * 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // validation goes here

        $user->likes = $request->likes;
        $user->save();

        return response()->json($user, 200);
    }
}
