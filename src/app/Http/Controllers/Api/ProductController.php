<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Requests\Product\AddPropertiesRequest;
use App\Http\Requests\Product\ArchiveProductRequest;
use App\Http\Requests\Product\UpdateAttributeRequest;

class ProductController extends Controller
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->middleware('auth:sanctum')->except([]);
        $this->middleware('user.active');
        $this->repository = $repository;
    }


    /**
     * Get authenticated store list products
     */
    public function list()
    {
        try {
            return response()->json(
                $this->repository->getAll()
            );
            // $response = Cache::remember('products', 60, fn()=> $this->repository->getAll());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    /**
     * Get store product by ID
     */
    public function show(string $id)
    {
        try {
            $response = $this->repository->getById($id);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function search()
    {
        try {
            $response = $this->repository->search();
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }


    public function filter()
    {
        try {
            $response = $this->repository->filter();
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function archived()
    {
        try {
            $response = $this->repository->archived();
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function archive(ArchiveProductRequest $request)
    {
        try {
            $response = $this->repository->archive($request->validated());
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function unarchive(ArchiveProductRequest $request)
    {
        try {
            $response = $this->repository->archive($request->validated(), 0);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    /**
     * Create new product for authenticated store
     */
    public function create(CreateProductRequest $request)
    {
        try {
            $response = $this->repository->create(
                $request->validated()
            );
            return response()->json($response, 201);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }


    /**
     * Update product 
     */
    public function update(UpdateProductRequest $request, string $id)
    {
        $attrs = $request->validated();
        if (count($attrs) === 0) {
            return response()->json(['error' => 'Введите хотя бы одно поле товара.'], 422);
        }
        try {
            $response = $this->repository->update($attrs, $id);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function updateAttributes(UpdateAttributeRequest $request, string $id)
    {
        $attrs = $request->validated();
        if (count($attrs) === 0) {
            return response()->json(['error' => 'Введите хотя бы одно поле товара: [state, quantity, name, category_id ...].'], 422);
        }
        try {
            $response = $this->repository->updateAttributes($attrs, $id);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function getImages(string $id)
    {
        try {
            $response = $this->repository->getImages($id);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }

    public function getProperties(string $id)
    {
        try {
            $response = $this->repository->getGroupProperties($id);
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }


    public function stats()
    {
        try {
            $response = $this->repository->stats();
            return response()->json($response);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 400);
        }
    }


    protected function catch_errors()
    {

        if ($properties_errors = validate_product_group_properties()) {
            return response()->json(['errors' =>  array($properties_errors)], 422);
        }

        if ($status_errors = validate_propuct_status()) {
            return response()->json(['errors' =>  array($status_errors)], 422);
        }

        if ($stock_errors = validate_propuct_stock()) {
            return response()->json(['errors' =>  array($stock_errors)], 422);
        }
    }
}
