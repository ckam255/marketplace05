<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum')->except('download');
        $this->middleware('user.active')->except('download');
    }

    public function download($path)
    {
        if ($abspath = storage_path('app/documents/' . $path)) {
            return response()->download($abspath);
        }
        return response()->json("файл не существует: $path", 404);
    }
}
