<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GlobalController extends Controller
{

    protected function _addOrUpdateProperties($product, $fallback = false)
    {

        $properties = request()->get('properties');
        foreach ($properties as $property) {
            if (!empty($property['id'])) {
                if (empty($property['characteristic_id'])) {
                    throw new \Exception("характеристика поле [characteristic_id] не существует ");
                }
                if (!$chract = $product->characteristics->where('id', $property['characteristic_id'])->first()) {
                    throw new \Exception("характеристика [" . $property['characteristic_id'] . "] не найдена ");
                }
                if (!$prop =  $chract->properties()->where('id', $property['id'])->first()) {
                    throw new \Exception("Своиство (" . $property['id'] . ") не найдена ");
                }
                $prop->update([
                    'name' => $property['name'],
                    'type' => $property['type'],
                ]);
                $this->_addPropertyValues($property['values'], $prop);
            } else {
                if (!empty($property['characteristic_id'])) {
                    if (!$chract = $product->characteristics->where('id', $property['characteristic_id'])->first()) {
                        throw new \Exception("характеристика [" . $property['characteristic_id'] . "] не найдена ");
                    }
                } else {
                    $chract = $product->characteristics->first();

                    $chract = $chract ?? $product->characteristics()->create([
                        'name' => 'Общие характеристики'
                    ]);
                }
                $prop = $chract->properties()->create([
                    'name' => $property['name'],
                    'type' => $property['type'],
                ]);
                $this->_addPropertyValues($property['values'], $prop);
            }
        }
        return $fallback ? $product->characteristics : null;
    }


    /**
     * Add product characteristics 
     */
    protected function _addCharacteristics($product, $characteristics, $fallback = false)
    {

        foreach ($characteristics as $charac) {
            $newCharac = $product->characteristics()->create([
                'name' => $charac['name'],
            ]);
            // save properties
            if (array_key_exists('properties', $charac)) {
                $properties = $charac['properties'];
                foreach ($properties as $property) {
                    $prop = $newCharac->properties()->create([
                        'name' => $property['name'],
                        'type' => $property['type']
                    ]);
                    // save property values
                    $this->_addPropertyValues($property['values'], $prop);
                }
            }
        }
        return $fallback ? $product->characteristics : null;
    }

    /**
     * Update product status
     */
    protected function _addPropertyValues(array $data, $property, bool $rollback = false)
    {
        if (!empty($data)) {
            if ($rollback) {
                $property->values()->delete();
            }
            foreach ($data as $value) {
                $property->values()->create([
                    'name' => !empty($value['name']) ? $value['name'] : null,
                    'value' =>  $value['value']
                ]);
            }
        }
    }
}
