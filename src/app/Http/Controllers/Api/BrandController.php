<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\BrandRepository;
use Illuminate\Http\Request;

class BrandController extends Controller
{

    private BrandRepository $repository;

    public function __construct(BrandRepository $repository)
    {
        $this->middleware('auth:sanctum');
        $this->middleware('user.active');
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = $this->repository->list()->get();
        return response()->json($brands);
    }

    public function search(Request $request)
    {
        $q = $request->input('q');
        $brands = $this->repository->search($q)->get();
        return response()->json($brands);
    }
}
