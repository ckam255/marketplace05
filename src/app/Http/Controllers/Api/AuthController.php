<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\AuthRepository;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\RegisterRequest;

use OpenApi\Annotations as OA;

class AuthController extends Controller
{
    private AuthRepository $repo;

    public function __construct(AuthRepository $authRepository)
    {
        $this->middleware('auth:sanctum')->except('login', 'register',  'unauthorized', 'forgotPassword', 'resetPassword', 'confirm');
        $this->middleware('user.active')->except('register', 'login', 'unauthorized', 'confirm');
        $this->repo = $authRepository;
    }

    public function unauthorized()
    {
        return response()->json(['error' => 'Вы не авторизованы'], 401);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        try {
            $user = $this->repo->login($request->only('email', 'password', 'device_name'));
            return response()->json($user);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    public function register(RegisterRequest $request)
    {
        try {
            $user = $this->repo->register($request->validated());
            return response()->json($user);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }



    public function confirm(string $token)
    {
        try {
            $user = $this->repo->confirm($token);
            return response()->json($user);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    public function user()
    {
        try {
            return  response()->json($this->repo->currentUser());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    public function logout()
    {
        try {
            return  response()->json($this->repo->logout(), 204);
        } catch (\Exception $except) {
            return response()->json(['error' => $except->getMessage()], 400);
        }
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        try {
            return  response()->json($this->repo->forgotPassword($request->get('email')));
        } catch (\Exception $except) {
            return response()->json(['error' => $except->getMessage()], 400);
        }
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            return response()->json($this->repo->resetPassword($request->validated()));
        } catch (\Exception $except) {
            return response()->json(['error' => $except->getMessage()], 400);
        }
    }


    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            return  response()->json($this->repo->changePassword($request->validated()), 204);
        } catch (\Exception $except) {
            return response()->json(['error' => $except->getMessage()], 400);
        }
    }

    public function currentAccessToken()
    {
        try {
            return  response()->json($this->repo->currentAccessToken());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    public function refreshCurrentAccessToken()
    {
        try {
            return  response()->json($this->repo->refreshToken());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    public function accessTokens()
    {
        try {
            return  response()->json($this->repo->accessTokens());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }
}
