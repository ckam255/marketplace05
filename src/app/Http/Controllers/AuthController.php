<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AuthRepository;

class AuthController extends Controller
{
    private AuthRepository $repository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->middleware('auth')->except('login', 'attempt');
        $this->repository = $authRepository;
    }

    public function login()
    {
        if (auth()->check()) {
            return redirect()->route('dashboard');
        }
        return view('auth.login');
    }

    public function attempt(Request $request)
    {
        try {
            $this->repository->login($request->only('email', 'password'));
            return redirect()->back()->with('success', 'Signed successfully!');
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', 'Bad credentials');  // view('auth.login', ['error' => 'Bad credentials']);
        }
    }
}
