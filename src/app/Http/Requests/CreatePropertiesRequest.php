<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePropertiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'group' => 'required_unless:group,[]',
            // 'group' => 'present|array',
            'groups' => 'nullable|array|min:1',
            'groups.*.name' => 'required|string',
            'groups.*.properties' => 'required|array',
            'groups.*.properties.*.name' => 'required|string',
            'groups.*.properties.*.type' => 'required|string',
            'groups.*.properties.*.values' => 'required|array',
            'groups.*.properties.*.values.*.name' => 'nullable|string',
            'groups.*.properties.*.values.*.value' => 'required|string',
            'groups.*.properties.*.characteristic_id' => 'nullable|integer|min:1|exists:characteristics,id',


            'properties' => 'nullable|array|min:1',
            'properties.*.name' => 'required|string',
            'properties.*.type' => 'required|string',
            'properties.*.values' => 'required|array',
            'properties.*.values.*.name' => 'nullable|string',
            'properties.*.values.*.value' => 'required|string',
            'properties.*.characteristic_id' => 'nullable|integer|min:1|exists:characteristics,id'

        ];
    }
}
