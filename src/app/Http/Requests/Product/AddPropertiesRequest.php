<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class AddPropertiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'properties' => 'required|array',
            'properties.*.is_value_id' => 'boolean',
            'properties.*.value' => 'required',
            // 'properties.*.group_id' => 'nullable|integer|min:1|exists:characteristics,id',
            'properties.*.property_id' => 'nullable|string|min:1|exists:properties,id'
        ];
    }
}
