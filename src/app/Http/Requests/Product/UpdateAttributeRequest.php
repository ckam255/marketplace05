<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|min:2',
            'slug' => 'nullable|string|min:2',
            'barcode' => 'nullable|string|min:8',
            'description' => 'nullable|string|min:4',
            'price' => 'nullable|numeric|min:1',
            'weight' => 'nullable|numeric|min:0',
            'length' => 'nullable|numeric|min:0',
            'height' => 'nullable|numeric|min:0',
            'width' => 'nullable|numeric|min:0',
            'vat' => 'nullable|integer|min:0',
            // 'isvisible' => 'boolean',
            // 'archived' => 'boolean',
            'brand_id' => 'nullable|string|min:1|exists:brands,id',
            'category_id' => 'nullable|string|min:1|exists:categories,id',
            // stock
            'quantity' => 'nullable|integer|min:0',
            // status
            'state' => 'nullable|string|min:1',
        ];
    }
}
