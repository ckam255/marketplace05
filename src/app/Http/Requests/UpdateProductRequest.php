<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|min:2',
            'slug' => 'nullable|string|min:2',
            'barcode' => 'nullable|string|min:8',
            'description' => 'nullable|string|min:4',
            'price' => 'nullable|numeric|min:1',
            'weight' => 'nullable|numeric|min:0',
            'length' => 'nullable|numeric|min:0',
            'height' => 'nullable|numeric|min:0',
            'width' => 'nullable|numeric|min:0',
            'vat' => 'nullable|integer|min:0',
            // 'isvisible' => 'boolean',
            // 'archived' => 'boolean',
            'quantity' => 'nullable|integer|min:1',
            'brand_id' => 'nullable|string|min:1|exists:brands,id',
            'category_id' => 'nullable|string|min:1|exists:categories,id',
            // properties
            'properties' => 'array',
            'properties.*.value' => 'required',
            'properties.*.is_value_id' => 'boolean',
            'properties.*.property_id' => 'required|string|min:1|exists:properties,id',
            //images
            'images' => 'array',
            'images.*' => 'required|integer|exists:images,id'
        ];
    }
}
