<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'contact' => 'required|string',
            'website' => 'required|string',
            'address' => 'required',

            'legal.name' => 'nullable|string',
            'legal.address' => 'required',
            'legal.form' => 'required|string',
            'legal.director' => 'required|string',
            'legal.inn' => 'required|string',
            'legal.bik' => 'required|string',
            'legal.kpp' => 'required|string',
            'legal.ogrn' => 'required|string',
            'legal.ogrnip' => 'required|string',
            'legal.payment_account' => 'required|string'
        ];
    }
}
