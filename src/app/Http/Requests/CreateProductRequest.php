<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'id' => 'nullable|min:2|unique:products',
            'name' => 'string|required|min:2',
            'slug' => 'string|required',
            'barcode' => 'nullable|string',
            'description' => 'nullable|string',
            'price' => 'numeric|required|min:0',
            'weight' => 'numeric|required|min:0',
            'length' => 'numeric|required|min:0',
            'height' => 'numeric|required|min:0',
            'width' => 'numeric|required|min:0',
            'vat' => 'numeric|required|min:0',
            'quantity' => 'nullable|integer|min:1',
            // 'store_id' => 'integer|required|min:1',
            'brand_id' => 'nullable|exists:brands,id',
            'category_id' => 'required|exists:categories,id',
            // properties
            'properties' => 'array',
            'properties.*.value' => 'required',
            'properties.*.is_value_id' => 'boolean',
            'properties.*.property_id' => 'required|exists:properties,id',
            //images
            'images' => 'array',
            'images.*' => 'required|integer|exists:images,id'
        ];
    }
}
