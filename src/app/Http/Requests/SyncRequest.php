<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SyncRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brands' => 'nullable|array',
            'properties' => 'nullable|array',
            'property_value' => 'nullable|array',
            'categories' => 'nullable|array',
            'category_properties_template' => 'nullable|array',
            'tariff' => 'nullable|array',
            'stores' => 'nullable|array',
            'products' => 'nullable|array',
            'product_property_value' => 'nullable|array',
            'amount' => 'nullable|array',
            'prices' => 'nullable|array',
            'calculations' => 'nullable|array',
            'balance' => 'nullable|array',
            'sales' => 'nullable|array',

        ];
    }
}
