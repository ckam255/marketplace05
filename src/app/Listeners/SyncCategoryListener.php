<?php

namespace App\Listeners;

use App\Events\SyncCategoryEvent;
use Illuminate\Support\Facades\Http;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncCategoryListener implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CategoryCreated  $event
     * @return void
     */
    public function handle(SyncCategoryEvent $event)
    {
        $response =  Http::post(env('1C_URL') . '/api/categories', $event->category->toArray());
        if ($response->successful()) {
            return info(printf($event->category->id . ' - ' . $event->category->name . ' has been successfully synchonized'));
        }
        return $response->throw()->json();
    }
}
