<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\PasswordReset;
use App\Events\RegistredEvent;
use App\Mail\RegistrationMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistedListener implements ShouldQueue
{




    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  RegistredEvent  $event
     * @return void
     */
    public function handle(RegistredEvent $event)
    {
        $token = Str::random(80);

        PasswordReset::create([
            'email' => $event->email,
            'token' => $token,
            'created_at' => now()
        ]);
        Mail::to($event->email)->queue((new RegistrationMail($token)));
    }

    private function getUniqueCode()
    {
        $code = mt_rand(20000000, 99999999);
        if ($exist = \App\Models\PasswordReset::where('token', $code)->first()) {
            return $this->getUniqueCode();
        }
        return $code;
    }
}
