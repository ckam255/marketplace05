<?php

namespace App\Listeners;



use App\Models\Product;
use App\Events\SyncProductEvent;
use Illuminate\Support\Facades\Http;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\Sync\SyncProductService;
use App\Services\Sync\SyncPropertyService;
use Illuminate\Contracts\Queue\ShouldQueue;

class SyncProductListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncProductEvent  $event
     * @return void
     */
    public function handle(SyncProductEvent $event)
    {
        SyncProductService::syncTo($event->products);
    }
}
