<?php

namespace App\Listeners;

use App\Events\SyncStoreEvent;
use App\Services\Sync\SyncStoreService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SyncStoreListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncStoreEvent  $event
     * @return void
     */
    public function handle(SyncStoreEvent $event)
    {
        sleep(5);
        SyncStoreService::syncTo($event->stores);
    }
}
