<?php

namespace App\Listeners;

use Illuminate\Support\Str;
use App\Mail\ResetPasswordMail;
use App\Events\ResetPasswordEvent;
use Illuminate\Support\Facades\DB;
use App\Events\ForgotPasswordEvent;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  ForgotPasswordEvent  $event
     * @return void
     */
    public function handle(ResetPasswordEvent $event)
    {
        $token = Str::random(255);
        PasswordReset::create([
            'email' => $event->email,
            'token' => $token,
            'created_at' => now()
        ]);
        Mail::to($event->email)
            ->queue((new ResetPasswordMail($token)));
    }
}
