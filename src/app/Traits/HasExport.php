<?php


namespace App\Traits;


trait HasExport
{
    public function export($node = null, array $exportable = []) : string
    {
        set_time_limit(-1);
        $data = count($exportable) === 0 ? $this->exportable() : $exportable;
        $output = $node === null ? $this->exportable() : [$node => [$data]];
        $json = json_encode($output);
        $path = export($json, "sync/" . file_gen_name($node, 'json'));
        return $path;
    }
}
