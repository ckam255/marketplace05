<?php

namespace App\Exceptions;

use Exception;
use PDOException;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    //use ExceptionTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {

        $this->renderable(function (QueryException $e, $request) {
            if ($request->expectsJson()) {
                return response()->json(
                    ['error' => 'К сожалению, Объект не найден (QueryException)'],
                    Response::HTTP_NOT_FOUND
                );
            }
            return parent::render($request, $e);
        });

        $this->renderable(function (ModelNotFoundException $e, $request) {
            if ($request->expectsJson()) {
                return response()->json(
                    ['error' => 'К сожалению, Объект не найден (ModelNotFoundException)'],
                    Response::HTTP_NOT_FOUND
                );
            }
            return parent::render($request, $e);
        });
    }
}
