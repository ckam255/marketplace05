<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Exception\NotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait ExceptionTrait 
{
    
    
    public function apiException($request, $exception)
    {
        if($this->IsModel($exception)){
            return response()->json(
                ['error' => 'К сожалению, Объект не найден'], 
                Response::HTTP_NOT_FOUND
            );
        }

        if($exception instanceof NotFoundException){
            return response()->json(
                ['error' => 'К сожалению, Неправильный маршрут'], 
                Response::HTTP_NOT_FOUND
            ); 
        }

        if($this->IsValidation($exception)){
            return response()->json(
                ['error' => 'К сожалению, Вы не авторизованы'], 
                422
            ); 
        }

        if($this->IsAuthentication($exception)){
            return response()->json(
                ['error' => 'К сожалению, проверка не удалась.'], 
                Response::HTTP_UNAUTHORIZED
            ); 
        }
        
    }

    public function IsModel($exception)
    {
        return $exception instanceof ModelNotFoundException;
    }

    public function IsHttp($exception)
    {
        return $exception instanceof NotFoundException;
    }

    public function IsValidation($exception)
    {
        return $exception instanceof ValidationException;
    }

    public function IsAuthentication($exception)
    {
        return $exception instanceof AuthenticationException;
    }


}