<?php





use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Api\StoreController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Admin\SyncDataController;
use App\Http\Controllers\Api\FileController;

/*
|--------------------------------------------------------------------------
| Documentation routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'docs'], function () {
    Route::get('/', [PageController::class, 'docs']);
    Route::get('/swagger', [PageController::class, 'swagger']);
    Route::get('/routes', [PageController::class, 'routes']);
});

/*
|--------------------------------------------------------------------------
| Admin & Synchornization routes
|--------------------------------------------------------------------------
|
*/
Route::group(['prefix' => 'admin',  /* 'middleware' => 'user.admin' */], function () {
    Route::post('sync/all', [SyncDataController::class, 'all']);
    Route::post('sync/brands', [SyncDataController::class, 'brands']);
    Route::post('sync/tariffs', [SyncDataController::class, 'categories']);
    Route::post('sync/categories', [SyncDataController::class, 'tariffs']);
    Route::post('sync/stores', [SyncDataController::class, 'stores']);
    Route::post('sync/products', [SyncDataController::class, 'products']);
    Route::post('sync/properties', [SyncDataController::class, 'properties']);
    Route::post('sync/orders', [SyncDataController::class, 'orders']);
});

/*
|--------------------------------------------------------------------------
| Authentication routes
|--------------------------------------------------------------------------
|
*/
Route::get('/', function () {
    return redirect('login');
})->name('api-home');
Route::get('/unauthorized', [AuthController::class, 'unauthorized'])->name('unauthorized');
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login'])->name('auth.login');
    Route::post('/register', [AuthController::class, 'register'])->name('auth.register');
    Route::get('/confirm/{token}', [AuthController::class, 'confirm'])->name('auth.confirm');
    Route::match(['post', 'get'], '/logout', [AuthController::class, 'logout'])->name('auth.logout');
    Route::match(['post', 'get'], '/user', [AuthController::class, 'user'])->name('auth.user');
    Route::get('user/token', [AuthController::class, 'currentAccessToken']);
    Route::get('user/token/refresh', [AuthController::class, 'refreshCurrentAccessToken']);
    Route::get('user/tokens', [AuthController::class, 'accessTokens'])->middleware('user.admin');

    Route::group(['prefix' => 'password'], function () {
        Route::post('/forgot', [AuthController::class, 'forgotPassword'])->name('auth.password.forgot');
        Route::post('/reset', [AuthController::class, 'resetPassword'])->name('auth.password.reset');
        Route::match(['post', 'patch'], '/change', [AuthController::class, 'changePassword'])->name('auth.password.change');
    });
});

/*
|--------------------------------------------------------------------------
| Categories routes
|--------------------------------------------------------------------------
|
*/
Route::get('/categories', [CategoryController::class, 'index'])->name('categories.list');
Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
Route::get('/categories/search', [CategoryController::class, 'search'])->name('categories.search');
Route::get('/categories/{id}', [CategoryController::class, 'show'])->name('categories.show');
Route::put('/categories/{id}', [CategoryController::class, 'update'])->name('categories.update');


/*
|--------------------------------------------------------------------------
| brands routes
|--------------------------------------------------------------------------
|
*/
Route::get('/brands', [BrandController::class, 'index'])->name('brands.index');
Route::get('/brands/search', [BrandController::class, 'search'])->name('brands.search');



/*
|--------------------------------------------------------------------------
| Products routes
|--------------------------------------------------------------------------
|
*/
Route::group(['prefix' => 'products'], function () {
    Route::get('/', [ProductController::class, 'list'])->name('products.list');
    Route::get('/search', [ProductController::class, 'search'])->name('products.search');
    Route::get('/filter', [ProductController::class, 'filter'])->name('products.filter');
    Route::get('/stats', [ProductController::class, 'stats'])->name('products.stats');
    Route::get('/archives', [ProductController::class, 'archived'])->name('products.archived');
    Route::post('/archive', [ProductController::class, 'archive'])->name('products.archive');
    Route::post('/unarchive', [ProductController::class, 'unarchive'])->name('products.unarchive');
    Route::post('/', [ProductController::class, 'create'])->name('products.create');
    Route::get('/{id}', [ProductController::class, 'show'])->name('products.show');
    Route::put('/{id}', [ProductController::class, 'update'])->name('products.update');
    Route::patch('/{id}', [ProductController::class, 'updateAttributes'])->name('products.update.attrs');
    Route::post('/images', [ImageController::class, 'upload'])->name('products.images.upload');
    Route::delete('/images/{id}', [ImageController::class, 'delete'])->name('products.images.delete');
    // Route::get('/{id}/properties', [ProductController::class, 'getProperties'])->name('products.properties.list');
    // Route::get('/{id}/images', [ProductController::class, 'getImages'])->name('products.images.list');
});

/*
|--------------------------------------------------------------------------
| Store routes
|--------------------------------------------------------------------------
|
*/
Route::group(['prefix' => 'stores'], function () {
    Route::post('/', [StoreController::class, 'create']);
    Route::get('/orders', [StoreController::class, 'orders']);
    Route::get('/orders/search', [StoreController::class, 'searchOrders']);
    Route::get('/orders/{id}', [StoreController::class, 'getOrderById']);
    Route::get('/accounting', [StoreController::class, 'amounts']);
    Route::get('/documents', [StoreController::class, 'documents']);
    Route::post('/documents', [StoreController::class, 'uploadDocument']);
});

// Route::get('/images/{width}/{height}/{path}', [ImageController::class, 'show'])->where('path', '.*');
Route::get('/images/{path}', [ImageController::class, 'display'])->where('path', '.*');
Route::get('/documents/{path}', [FileController::class, 'download'])->where('path', '.*');
