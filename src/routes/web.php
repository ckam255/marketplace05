<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\PageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/attempt', [AuthController::class, 'attempt'])->name('attempt');




Route::get('/', [PageController::class, 'welcome']);
Route::get('/dashboard', [PageController::class, 'dashboard'])->name('dashboard');


Route::group(['prefix' => 'admin'], function () {
});
