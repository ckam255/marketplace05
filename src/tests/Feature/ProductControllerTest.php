<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    private array $headers = [
        'Authorization' => 'Bearer 4|94g6gzd5Vdov2hDXQoxNS7Fc0zToj12P9I9s9ziT',
        'Accept' => 'application/json'
    ];
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testNotAllowFetchProducts()
    {
        $response = $this->getJson('/api/products');
        $response->assertUnauthorized();
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testListProducts()
    {
        $response = $this->getJson('/api/products/?per_page=5', $this->headers);
        $response->assertStatus(200);
    }

            /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPaginationProducts()
    {
        $response = $this->getJson('/api/products/?per_page=10', $this->headers);
        $response->assertJsonCount(10);
    }

    public function testValidationCreatingProduct()
    {
        $response = $this->postJson('/api/products', [
        ], $this->headers);
        $response->assertStatus(422);
    }

    public function testCreateProduct()
    {
        $response = $this->postJson('/api/products', [
            'name' => 'ASEC',
            'slug' => 'asec',
            'barcode' => '128799555',
            'description' => 'my test product',
            'price' => 999.09,
            'weight' => 14.32,
            'length' => 85,
            'height' => 102,
            'width' => 55,
            'vat' => 7,
            'quantity' => 120,
            'category_id' => '00000000780'
        ], $this->headers);
        $response->assertCreated();
    }

    public function testSearchProducts()
    {
        $response = $this->getJson('/api/brands/products/?q=ASEC', $this->headers);
        $response->assertJsonCount(1);
    }
}
