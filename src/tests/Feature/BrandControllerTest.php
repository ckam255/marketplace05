<?php

namespace Tests\Feature;

use App\Models\Brand;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class BrandControllerTest extends TestCase
{
    private array $headers = [
        'Authorization' => 'Bearer 4|94g6gzd5Vdov2hDXQoxNS7Fc0zToj12P9I9s9ziT',
        'Accept' => 'application/json'
    ];
    // public function setUp():void
    // {
    //     parent::setUp();
    //     Artisan::call('migrate');
    // }

    // /**
    //  * A basic feature test example.
    //  *
    //  * @return void
    //  */
    // public function testCreateAndCountBrands()
    // {
    //     Brand::create(['name' => 'LG', 'description' => ' korean company']);
    //     $this->assertEquals(1, Brand::count());
    // }

    // public function testCreateAndGetBrands()
    // {
    //     Brand::create(['name' => 'Samsung', 'description' => ' korean company']);
    //     $brand = Brand::first();
    //     $this->assertEquals('Samsung', $brand->name);
    // }

    public function testNotAllowFetchBrands()
    {
        $response = $this->getJson('/api/brands');
        $response->assertUnauthorized();
    }


        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testListBrands()
    {
        $response = $this->get('/api/brands', $this->headers);

        $response->assertStatus(200);
    }

    public function testPaginationBrands()
    {
        $response = $this->getJson('/api/brands/?per_page=5', $this->headers);
        $response->assertJsonCount(5);
    }


    public function testSearchBrands()
    {
        $response = $this->getJson('/api/brands/search/?q=LG', $this->headers);
        $response->assertJsonCount(5);
    }


}
