<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Маркетплейс- @yield('title')</title>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;700&display=swap" rel="stylesheet">
    <style>
        *{
            font-family: 'Ubuntu', sans-serif;
            box-sizing: border-box;
            padding: 0;
            margin: 0;
        }
        .login{
            background: #242424;
            position: fixed;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .container{
            display: flex;
            flex-direction: column;
            align-items: center;
        }
      
        .logo{
            width: 96px;
            height: 52px;
            margin-bottom: 8px;
        }
        .login-header{
            font-family: Ubuntu;
            font-style: normal;
            font-weight: 500;
            font-size: 24px;
            line-height: 28px;
            color: #FFFFFF;
            margin-bottom: 32px;
        }
        .form{
            background: #FFFFFF;
            border: 1px solid #E5E5E5;
            box-sizing: border-box;
            border-radius: 4px;
            width: 458px;
            padding: 40px;
            
            display: flex;
            flex-direction: column;
        }
        .form h1{
            font-family: Ubuntu;
            font-style: normal;
            font-weight: 500;
            font-size: 29px;
            line-height: 32px;
            color: #000000;
            text-align: left;
        }
        .form label{
            font-family: Ubuntu;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 16px;
            color: #1A1A1A;
            padding-bottom: 5px;
        }
        .form a{
            text-decoration: none;
            font-family: Ubuntu;
            font-style: normal;
            font-weight: normal;
            font-size: 13px;
            line-height: 15px;
            color: #000000;
        }
        .form input{
            outline: none;
            background: #FFFFFF;
            border: 1px solid #E5E5E5;
            box-sizing: border-box;
            border-radius: 4px;
            width: 378px;
            height: 39px;
            padding: 0 10px ;
        }
        .form input:focus{
            border-color: #E30613;
        }
        .form button{
            outline: none;
            border: none;
            font-family: Ubuntu;
            font-style: normal;
            font-weight: 500;
            font-size: 20px;
            line-height: 23px;
            padding: 12 27px;
            text-align: center;
            color: #FFFFFF;
            background: #E30613;
            cursor: pointer;
            width: 114px;
            height: 49px;
            margin-top: 25px;
            border-radius: 4px;
        }
        .form-bloc{
            display: flex;
            justify-content: space-between;
        }
        .form-group{
            margin-top: 25px;
        }
        .error{
            background-color: #ecd8d9;
            color: #E30613;
            width: 100%;
            padding: 10px;
            margin: 10px 0;
        }
        .error_field{
            color: #E30613;
        }
    </style>
</head>
<body>
    <div class="login">
        <div class="container">
            <img src="{{ asset('img/logo.svg') }}" alt="" class="logo" > 
            <h1 class="login-header">Маркетплейс</h1>
           @yield('content')
        </div>
    </div>
</body>
</html>