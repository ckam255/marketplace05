<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div style="background-color: #f5f5f5">
        <div class="container" style="margin: 60px auto; 
        background: #ffffff;width: 40%;height: 50vh;
        box-shadow: 1px 2px 4px rgba(0, 0, 0, 0.3);
        border: 1xp solid #ccc;
         padding: 50px;">
            <div>
                <div style="color: #000000">Для подтверждения регистрации аккаунта продавца на маркетплейсе <a href="https://05.ru">05.ru</a> перейдите по этой ссылке. 
                    Если вы получили данное письмо по ошибке, просто проигнорируйте его.</div><br/><br/>
                <a style="padding: 10px; background: #dddddd; 
                color: #000000;text-decoration: none" 
                href="{{ env('APP_URL', 'https://mp.05.ru')}}/?code={{ $token }}">
                Подтвердите аккаунт
                </a>
            </div>
            <div style="margin-top: 50px; width: 100%">
                С уважением<br/>
                <a href="https://05.ru">05.ru</a>
            </div>
        </div>
    </div>
</body>
</html>