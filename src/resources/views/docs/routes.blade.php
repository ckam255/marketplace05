<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
    {{-- <h1 style="text-align: center;">все роуты</h1>
    <table>
        <thead>
            <th>Domain</th>
            <th>Method</th>
            <th>URI</th>
            <th>Description</th>
            <th>Parameters</th>
            <th>Headers</th>
        </thead>
        <tbody>
            <tr><td colspan="6" class="td_header">Categories</td></tr>
            <tr>
                <td rowspan="13"><a href="https://mp.05.ru/api">mp.05.ru/api</a></td>
                <td  class="get_method">GET</td>
                <td><pre>/categories</pre></td>
                <td>List categories</td>
                <td><em style="color: coral;"></td>
                <td rowspan="13">
                    <span><em style="color: rgb(26, 198, 228);">Accept: </em>application/json</span><br/>
                    <span><em style="color: rgb(26, 198, 228);">Authorization: </em>Bearer [<em>token</em>]</span>
                </td>
            </tr>
            <tr>
                <td  class="post_method">POST</td>
                <td><pre>/categories</pre></td>
                <td>Create categories</td>
                <td><em style="color: coral;"></td>
            </tr>
            <tr>
                <td  class="put_method">PUT</td>
                <td><pre>/categories/{id}</pre></td>
                <td>Update categories</td>
                <td><em style="color: coral;"></td>
            </tr>
            <tr>
                <td  class="patch_method">PATCH</td>
                <td><pre>/categories/{id}</pre></td>
                <td>Update categories</td>
                <td><em style="color: coral;"></td>
            </tr>
            <tr>
                <td  class="delete_method">DELETE</td>
                <td><pre>/categories/{id}</pre></td>
                <td>Delete categories</td>
                <td><em style="color: coral;"></td>
            </tr>


            <tr><td colspan="4" class="td_header">Brands</td></tr>
            <tr>
                <td  class="get_method">GET</td>
                <td><pre>/brands</pre></td>
                <td>List brands</td>
                <td><em style="color: coral;"></td>
            </tr>
        </tbody>
    </table> --}}


    <h1 style="text-align: center;">СИНХРОНИЗАЦИЯ С 1С</h1>
    <table>
        <thead>
            <th>Domain</th>
            <th>Method</th>
            <th>URI</th>
            <th>Description</th>
            <th>Parameters</th>
            <th>Headers</th>
        </thead>
        <tbody>
            <tr>
                <td rowspan="8"><a href="https://test.mp.05.ru/api">test.mp.05.ru/api</a></td>
                <td rowspan="8" class="post_method">POST</td>
                <td><pre>/admin/sync/all</pre></td>
                <td>Все данные</td>
                <td rowspan="8">
                    <em style="color: coral;">brands: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">properties: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">property_value: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">categories: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">category_properties_template: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">tariff: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">stores: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">products: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">product_property_value: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">amount: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">prices: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">calculations: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">balance: </em><b class="param_type"> nullable | array</b><br/>
                    <em style="color: coral;">sales: </em><b class="param_type"> nullable | array</b><br/>
                </td>
                <td rowspan="8">
                    <span><em style="color: rgb(26, 198, 228);">Content-Type: </em>application/json</span><br/>
                    <span><em style="color: rgb(26, 198, 228);">Accept: </em>application/json</span><br/>
                    <span><em style="color: rgb(26, 198, 228);">Authorization: </em>Bearer [<em>token</em>]</span><br/>
                    <span><em style="color: rgb(26, 198, 228);">Authorization: </em>Basic [<em>usename/password</em>]</span>
                </td>
            </tr>
            <tr>
                <td><pre>/admin/sync/brands</pre></td>
                <td>Бренды</td>
            </tr>
            <tr>
                <td><pre>/admin/sync/categories</pre></td>
                <td>категории,категории-свойства-шаблон</td>
            </tr>
            <tr>
                <td><pre>/admin/sync/tariffs</pre></td>
                <td>Тариффы</td>
            </tr>
            <tr>
                <td><pre>/admin/sync/products</pre></td>
                <td>Товары, Цены, характеристики Товаров </td>
            </tr>
            <tr>
                <td><pre>/admin/sync/properties</pre></td>
                <td>характеристики и значения<td>
            </tr>
            <tr>
                <td><pre>/admin/sync/stores</pre></td>
                <td>Партнеры, Балансы,Расчеты </td>
            </tr>
            <tr>
                <td><pre>/admin/sync/orders</pre></td>
                <td>Заказы </td>
            </tr>
        </tbody>
    </table>
    <style>
        h1{
            /* color: #E30613; */
        }
        body{
            background: #242424;
            color:  rgba(255, 255, 255, 0.8);
        }
        a{
            color: inherit;
            text-decoration: none;
        }
        a:hover{
            text-decoration: underline;
        }
        table{
            width: 70%;
            margin: 0 auto;
            background-color: rgba(255, 255, 255, 0.1);
            border-collapse: collapse;
        }
        th,td{
            border-bottom: 1px solid #ccc;
            margin-bottom: 2px;
            text-align: center;
        }
        td{
            border-right: 1px solid #ccc;
            text-align: justify;
            padding-left: 15px;
        }
        .get_method{
            color:  rgb(112, 235, 31);
        }
        .post_method{
            color:  rgb(26, 198, 228);
        }
        .put_method{
            color:  rgb(238, 157, 52);
        }
        .patch_method{
            color:  rgb(101, 52, 238);
        }
        .delete_method{
            color:  rgb(236, 55, 140);
        }
        .td_header{
            text-align: center
        }
    </style>
</body>
</html>