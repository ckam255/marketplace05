@extends('layouts.auth')

@section('title') 
Login
@endsection('title')

@section('content')

<form action="{{ url('attempt') }}" method="POST" class="form" >
    {{ csrf_field() }}
   @if(session('error'))
    <div class="error">
       <li>{{ session('error') }}</li>
    </div>
   @endif
    <h1>Вход</h1>
    
    <div class="form-group">
        <div class="form-bloc">
            <label for="email">Имейл:</label>
         </div>
        <input type="email" name="email" id="email" required>
        @if($errors->has('email'))
        <small class="error_field">{{ $errors->first('email') }}</small>
        @endif
    </div>

  <div class="form-group">
    <div class="form-bloc">
        <label for="password">Пароль:</label>
        {{-- <a href="{{ route('register') }}">Забыли пароль?</a> --}}
     </div>
      <input type="password" name="password" id="password" required>
  </div>

    <button class="btn-submit" type="submit">Войти</button>
</form>
@endsection('content')