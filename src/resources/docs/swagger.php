<?php

use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="marketplace API Doc",
 *      description="05.ru marketplace API Doc",
 * )
 * @OA\Server(
 *   url="/api",
 *   description="v1" 
 * )
 * @OA\Components(
 *    @OA\Schema(
 *     schema="User",
 *     required={"name", "email", "password"},
 *     @OA\Xml(name="User"),
 *     @OA\Property(type="integer", format="int64", property="id"),
 *     @OA\Property(type="string", property="name"),
 *     @OA\Property(type="string", property="email"),
 *     @OA\Property(type="string", property="password"),
 *     @OA\Property(type="boolean", property="is_active"),
 *     @OA\Property(type="boolean", property="is_admin"),
 *     @OA\Property(type="boolean", property="is_superuser"),
 *     @OA\Property(type="datetime", property="created_at"),
 *     @OA\Property(type="datetime", property="updated_at"),
 *    )
 * )
 */
