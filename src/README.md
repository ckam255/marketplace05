# Marketplace

## DOCUMENTATION API

```bash
# zircote/swagger
cd src && ./vendor/bin/openapi --format json --output ./public/swagger.json ./resources/docs/swagger.php app
```

## Redis

Prerequisites

Redis: predis/predis ~1.0 or phpredis PHP extension

```bash
composer require predis/predis
sudo apt-get install php-redis
```

In order to use the redis queue driver, you should configure a Redis database connection in your `config/database.php` configuration file.

```php
'redis' => [
    'driver' => 'redis',
    'connection' => 'default',
    'queue' => '{default}',
    'retry_after' => 90,
],
```

###### 2. Configuration Redis

The Redis configuration for your application is located in the `config/database.php` configuration file. Within this file, you will see a `redis` array containing the Redis servers utilized by your application:

```php
'redis' => [

    'client' => env('REDIS_CLIENT', 'phpredis'),

    'default' => [
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'password' => env('REDIS_PASSWORD', null),
        'port' => env('REDIS_PORT', 6379),
        'database' => env('REDIS_DB', 0),
    ],

    'cache' => [
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'password' => env('REDIS_PASSWORD', null),
        'port' => env('REDIS_PORT', 6379),
        'database' => env('REDIS_CACHE_DB', 1),
    ],

],
```

## Supervisor Configuration

###### 1. Installing Supervisor

Supervisor is a process monitor for the Linux operating system, and will automatically restart your `queue:work` process if it fails. To install Supervisor on Ubuntu, you may use the following command:

```bash
sudo apt-get install supervisor
```

###### 2. Configuring Supervisor

Supervisor configuration files are typically stored in the `/etc/supervisor/conf.d` directory. Within this directory, you may create any number of configuration files that instruct supervisor how your processes should be monitored. For example, let's create a `mp-worker.conf` file that starts and monitors a `queue:work` process:

```bash
[program:mp-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /home/ckam/workspace/projects/05.ru/mp/artisan queue:work  --sleep=>
autostart=true
autorestart=true
user=ckam
numprocs=8
redirect_stderr=true
stdout_logfile=/home/ckam/workspace/projects/05.ru/mp/storage/logs/worker.log
stopwaitsecs=3600
```

###### 3. Starting Supervisor

```bash
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start mp-worker:*
```

## Horizon

Queue job monitorin application for laravel

###### Commands

```bash
php artisan horizon
php artisan horizon:pause
php artisan horizon:continue
php artisan horizon:status
php artisan horizon:terminate

php artisan horizon:pause-supervisor supervisor-1

php artisan horizon:continue-supervisor supervisor-1

sudo supervisorctl start horizon
```

pervisor configuration files are typically stored in the `/etc/supervisor/conf.d` directory. Within this directory, you may create any number of configuration files that instruct supervisor how your processes should be monitored. For example, let's create a `horizon.conf` file that starts and monitors a horizon process:

```bash
[program:horizon]
process_name=%(program_name)s
command=php /home/forge/app.com/artisan horizon
autostart=true
autorestart=true
user=forge
redirect_stderr=true
stdout_logfile=/home/forge/app.com/horizon.log
stopwaitsecs=3600
```
