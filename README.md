## Marketplace Project

#### Requirements

- REDIS: `statble`
- SUPERVISOR
- POSTGRESQL: `12`
- COMPOSER
- NGINX
- PHP: `7.4`
- PHP-FPM

#### Docker containers

- web: nginx server
- db: Postgresql database
- fmp: Laravel app(PHP-FPM)
- redis
- supervisor
- artisan

#### copy .env file

```bash
cp src/.env.example src/.env
```

#### Create symlink for Docker environment variables

```bash
ln -s src/.env .env
```

#### setup env variables

using `nano src/.env`

```bash
...
APP_ENV=production
APP_DEBUG=false
SYNC_1C_URL=http://1c.05.ru:86/1C_UT11_DEV_NEW/hs/api/site/marketplace/sync
SYNC_1C_USER=site05ru_user
SYNC_1C_PWD=

...

DB_DATABASE=...  # set database name that you
DB_USERNAME=...  # set database user that you
DB_PASSWORD=...  # set database password that you

....

# Async worker
REDIS_PASSWORD=... # set redis password that you


# setup SMTP Parameters for mail sending
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=noreply@05.ru
MAIL_FROM_NAME="${APP_NAME}"

# ONLY FOR PRODUCTION
DOCKER_PORT_PREFIX=2 # the externals docker containers will start to 2: localhost:2000
...

```

#### build project containers

```bash
docker-compose build
```

#### generate key

```bash
docker-compose run --rm artisan key:generate
```

#### migrate database

```bash
docker-compose run --rm artisan migrate
```

#### load status list

```bash
docker-compose run --rm artisan db:seed --class=StatusSeeder
```

#### run project

```bash
docker-compose up -d web
```

remove flag `-d` if want to show logs

#### Database console management

```bash
docker-compose run --rm  db psql -h db -U username -d dbname # postgres console
# or

```

## 2. Functionals commands

##### Load data for development

to avoid synchronization problems you have to execute these commands one after the other as below.

If flag `--target` not specified, then will be full synchronization

Flag `--file`: is optional to specify from which file in folder `storage/sync` well be synchronized datas. For example: --file=[filename].json

```bash

docker-compose run --rm  artisan db:load  # sync all data:
docker-compose run --rm  artisan db:load --target=brands
docker-compose run --rm  artisan db:load --target=properties
docker-compose run --rm  artisan db:load --target=categories
docker-compose run --rm  artisan db:load --target=tariffs
docker-compose run --rm  artisan db:load --target=stores
docker-compose run --rm  artisan db:load --target=products
docker-compose run --rm  artisan db:load --target=orders


```

##### Backup/Restore database

- Backup database
  By default dump database into `sql` fromat, but you change to json also by using `--type=json`

```bash
docker-compose run --rm  artisan db:dump
```

- Restore database
  By default restore database from `sql` fromat, but you change to json also by using `--type=json`
  `filename` will be in directory `src/storage/backup`

```bash
docker-compose run --rm  artisan db:restore filename
```

##### Clean logs and synchronization folder

- clean logs

```bash
docker-compose run --rm  artisan logs:clear {--f|file= : clear specify file log}
```

- clean synchronization folder

```bash
docker-compose run --rm  artisan sync:clear {--d|dir= : clear specify directory}
```

##### Create service class

```bash
docker-compose run --rm  artisan make:service {name}
```

##### Create repository class

```bash
docker-compose run --rm  artisan make:repository {name} {--m|model= : Create injected model for repository}
```

##### Create users in database

```bash
docker-compose run --rm  artisan create:user {name} {email} {password} {--role= : role}
```

`--role` is optional [superuser, admin]

### Others commands

- Show containers address

```bash
docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
```

- using bash command inside container

```bash
docker exec -u 0 -it [container_id] sh
```
